package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.ProjetPrestationAgence;
import com.backendFicheReference.backendFicheReference.model.pk.ProjetPrestationAgencePk;
import com.backendFicheReference.backendFicheReference.repository.ProjetPrestationAgenceRepository;
import com.backendFicheReference.backendFicheReference.service.ProjetPrestationAgenceService;
import com.backendFicheReference.exceptions.ProjetPrestationAgenceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProjetPrestationAgenceServiceImpl implements ProjetPrestationAgenceService {

    @Autowired
    private ProjetPrestationAgenceRepository projetPrestationAgenceRepository;

    @Override
    public Optional<ProjetPrestationAgence> findProjetPrestationAgence(ProjetPrestationAgencePk id) {
        final Optional<ProjetPrestationAgence> optionalProjetPrestationAgence = this.projetPrestationAgenceRepository.findById(id);

        if (optionalProjetPrestationAgence.isPresent()) {
            final ProjetPrestationAgence projetPrestationAgence = optionalProjetPrestationAgence.get();
            // convertir en ProjetPrestationAgenceDTO
            // final ProjetPrestationAgenceDTO dto = new ProjetPrestationAgenceDTO(projetPrestationAgence);
            // final ProjetPrestationAgenceDTO dto = ProjetPrestationAgenceUtils.convert(projetPrestationAgence, "findById");
            return projetPrestationAgenceRepository.findById(id);
        } else {
            throw ProjetPrestationAgenceNotFoundException.projetPrestationAgenceNotFound();
        }
    }

    @Override
    public ProjetPrestationAgence saveProjetPrestationAgence(ProjetPrestationAgence projetPrestationAgence) {
        return projetPrestationAgenceRepository.save(projetPrestationAgence);
    }

    @Override
    public ProjetPrestationAgence updateProjetPrestationAgence(ProjetPrestationAgence projetPrestationAgence, ProjetPrestationAgencePk projetPrestationAgencePk) {
        projetPrestationAgence.setId(projetPrestationAgencePk);
        return projetPrestationAgenceRepository.save(projetPrestationAgence);
    }


    @Override
    public Page<ProjetPrestationAgence> projetPrestationAgences(String page , String size, String sortedBy, String sortedIn){
        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return projetPrestationAgenceRepository.findAll(pageable);
    }

}