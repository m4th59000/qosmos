package com.backendFicheReference.backendFicheReference.repository;

import com.backendFicheReference.backendFicheReference.model.Projet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProjetRepository extends JpaRepository<Projet, Long> {
    Optional<Projet> findById(Long id);
    Page<Projet> findAll(Pageable pageable);
}