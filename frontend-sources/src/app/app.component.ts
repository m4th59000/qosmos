import { Component } from '@angular/core';

import {User} from "../shared/models/User.model";
import {Router} from "@angular/router";
import {AuthenticationService} from "../shared/_services";
import {AuthService} from "./NeoSoftBordeau2018/services/auth.service";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'fiches-references';

  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private authService: AuthService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  isAuth(): boolean {
    return this.authService.isAuth();
  }
}
