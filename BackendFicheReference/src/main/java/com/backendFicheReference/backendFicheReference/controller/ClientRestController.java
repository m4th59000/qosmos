package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.Client;
import com.backendFicheReference.backendFicheReference.service.ClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ClientRestController{
    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "clients/{id}", method = RequestMethod.GET)
    public Client findClient(@PathVariable("id") Long id){
        return clientService.findClient(id);
    }

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public Page<Client> listClients(@RequestParam(name = "page" , defaultValue = "0") String page
            , @RequestParam(name = "size" , defaultValue = "10")  String size
            , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
            , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){
        return clientService.clients(page,size,sortedBy,sortedIn);
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    public Client findClient(@RequestBody Client client){
        return clientService.saveClient(client);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    public Client updateClient(@RequestBody Client client , @PathVariable("id") @NotNull Long id ){
        return clientService.updateClient(client,id);
    }
}
