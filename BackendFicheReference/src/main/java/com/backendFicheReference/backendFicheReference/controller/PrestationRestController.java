package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.Prestation;
import com.backendFicheReference.backendFicheReference.service.PrestationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PrestationRestController{
    @Autowired
    private PrestationService prestationService;
    @RequestMapping(value = "prestations/{id}", method = RequestMethod.GET)
    public Optional<Prestation> findPrestation(@PathVariable("id") Long id){
        return prestationService.findPrestation(id);
    }

@RequestMapping(value = "/prestations", method = RequestMethod.GET)
  public Page<Prestation> listPrestations(@RequestParam(name = "page" , defaultValue = "0") String page
        , @RequestParam(name = "size" , defaultValue = "10")  String size
        , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
        , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){
      return prestationService.prestations(page , size,sortedBy , sortedIn);
}

@RequestMapping(value = "/prestations", method = RequestMethod.POST)
    public Prestation findPrestation(@RequestBody Prestation prestation){
        return prestationService.savePrestation(prestation);
}

    @RequestMapping(value = "/prestations/{id}", method = RequestMethod.PUT)
    public Prestation updatePrestation(@RequestBody Prestation prestation , @PathVariable("id") @NotNull Long id ){
        return prestationService.updatePrestation(prestation,id);
    }
}
