/**
 * Class which materialize a client
 */
export class Client {
    private _id: number;
    private _name: string;
    private _nombreCollaborateurs: number;

    constructor(id: number) {
        this._id = id;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get nombreCollaborateurs(): number {
        return this._nombreCollaborateurs;
    }

    set nombreCollaborateurs(value: number) {
        this._nombreCollaborateurs = value;
    }
}
