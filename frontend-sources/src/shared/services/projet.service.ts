import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Projet} from "../models/Projet.model";
import {environment} from "../../environments/environment";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";
import {Client} from "../models/Client.model";

const baseUrl = environment.endpoint;
const path = 'projets/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class ProjetService {

  constructor(private _http: HttpClient) { }
  getProjet(id: string) {
    return this._http.get<Projet>(baseUrl + path + id , {'headers': headers});
  }

  getProjets(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  createProjet(projet: Projet) {
    return this._http.post<Projet>(baseUrl + path , projet , {'headers': headers});
  }
  updateProjet(projet: Projet) {
    return this._http.put<Projet>(baseUrl + path , projet , {'headers': headers});
  }
}



