import {Methodologie} from './Methodologie.model';
import {Prestation} from './Prestation.model';

export class PrestationMethodologie {
  constructor(
    public methodologie?: Methodologie,
    public prestation?: Prestation
  ) {}
}
