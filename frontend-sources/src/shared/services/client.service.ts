import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Agence} from "../models/Agence.model";
import {Client} from "../models/Client.model";
import {environment} from "../../environments/environment";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";
import {Observable} from "rxjs";


const baseUrl = environment.endpoint;
const path = 'clients/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private _http: HttpClient) { }



  getClients(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  getClient(id: number):Observable<Client> {
    return this._http.get<Client>(baseUrl + path + id, {'headers': headers});
  }
  createClient(client: Client): Observable<Client> {
    return this._http.post<Client>(baseUrl + path , client , {'headers': headers});
  }
  updateAgence(client: Client) {
    return this._http.put<Client>(baseUrl + path , client , {'headers': headers});
  }
}

