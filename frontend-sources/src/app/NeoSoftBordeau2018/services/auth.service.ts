import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
/**
 * Service to manage the authentication
 */
export class AuthService {
  //true if authenticated, otherwise false
  public auth = true;

  /**
   * Constructor
   */
  constructor() { }

  /**
   * Activate the authentication
   */
  public activeAuth() {
    this.auth = true;
  }

  /**
   * Deactivate the authentication
   */
  public deactiveAuth() {
    this.auth = false;
  }

  /**
   * Check if the is authenticated
   */
  public isAuth(): boolean {
    return this.auth;
  }
}
