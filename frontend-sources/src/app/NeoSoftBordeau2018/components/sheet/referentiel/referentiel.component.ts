import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import { Sheet } from 'src/app/NeoSoftBordeau2018/models/sheet.model';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { MockSheetService } from 'src/app/NeoSoftBordeau2018/services/mock-sheet.service';
import { SheetCartService } from 'src/app/NeoSoftBordeau2018/services/sheet-cart.service';

@Component({
  selector: 'app-referentiel',
  templateUrl: 'referentiel.component.html',
  styleUrls: ['referentiel.component.scss'],
})
/**
 * Component class for Referentiel page managing
 */
export class ReferentielComponent implements OnInit, AfterViewInit {
  //All sheets objects
  sheets: Sheet[];

  //Agency list, used in the html select component 'agenceFilter'
  agences: string[] = ["Toutes les agences"];
  //Client list, used in the html select component 'clientFilter'
  clients: string[] = ["Tous les clients"];
  //Position list, used in the html select component 'posteFilter'
  postes: string[] = ["Tous les postes"];

  //Datatables options
  dtOptions: DataTables.Settings = {};

  //French datatable informations to set in the datatables options
  frenchLanguage: DataTables.LanguageSettings = {
    "processing":     "Traitement en cours...",
    "search":         "Rechercher&nbsp;:",
    "lengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
    "info":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "infoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
    "infoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    "infoPostFix":    "",
    "loadingRecords": "Chargement en cours...",
    "zeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    "emptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
    "paginate": {
        "first":      "Premier",
        "previous":   "Pr&eacute;c&eacute;dent",
        "next":       "Suivant",
        "last":       "Dernier"
    },
    "aria": {
        "sortAscending":  ": activer pour trier la colonne par ordre croissant",
        "sortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
    }/*,
    "select": {
            "rows": {
                _: "%d lignes séléctionnées",
                0: "Aucune ligne séléctionnée",
                1: "1 ligne séléctionnée"
            }
    }*/
  }

  /**
   * Datatable directive
   */
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;

  /**
   * Constructor
   * @param sheetService
   * @param router
   */
  constructor(private sheetService: MockSheetService,
              private sheetCartService: SheetCartService,
              private router: Router) {
  }

  /**
   * Initialisation of the component
   */
  ngOnInit() {
    //Set the language of the datatable to french
    this.dtOptions.language = this.frenchLanguage;

    //Get all sheets from the server and do a subscription on it
    this.sheetService.getAllSheets();
    this.sheetService.allSheets.subscribe(sheets => this.sheets = sheets);

    //Get all values from the sheet service for the list filter
    this.agences.push(...this.sheetService.getAllAgences());
    this.clients.push(...this.sheetService.getAllClients());
    this.postes.push(...this.sheetService.getAllPostes());
  }

  /**
   * Initialisation after Angular has fully initialized the component's view
   */
  ngAfterViewInit(): void {
    //Add listeners to the datatable column filters, in order to update the filtered values
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns().every(function () {
        const that = this;
        $('input', this.footer()).on('change keyup', function () {
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });
        $('select', this.footer()).on('change', function () {
          if (that.search() !== this['value']) {
            if(this['value'] == 'Toutes les agences' ||
               this['value'] == 'Tous les clients' ||
               this['value'] == 'Tous les postes') {
                that.search('').draw();
            } else {
              that
                .search(this['value'])
                .draw();
            }
          }
        });
      });
    });
  }

  /**
   * Redirect to the view sheet page
   * @param id The sheet id to view
   */
  onViewSheet(id: number) {
    this.router.navigate(['/sheets', 'view', id]);
  }

  /**
   * Redirect to the edit sheet page
   * @param id The sheet id to view
   */
  onEditSheet(id: number) {
    this.router.navigate(['/sheet/form'], { queryParams: { id: id } });
  }

  /**
   * Add or remove the selected sheet to the service selected list
   * @param id The sheet id to add
   */
  onSelectSheet(id: number) {
    //Search the sheet identified by the given id in the sheet list
    let sheet = this.sheets.find(item => item.id === id);

    if(sheet !== undefined) {
      //Get the found sheet index in the list
      let foundIndex = this.sheets.indexOf(sheet);

      //If the sheet is already selected
      if(sheet.selected) {
        //The sheet is remove from the service list and unselected from the current list
        if(this.sheetCartService.removeSheetFromselectedList(id)){
          sheet.selected = false;
          this.sheets[foundIndex] = sheet;
        }
      //If the sheet is not already selected
      } else {
        //The sheet is added to the service list and selected in the current list
        if(this.sheetCartService.addSheetToselectedList(id)){
          sheet.selected = true;
          this.sheets[foundIndex] = sheet;
        }
      }
    }
  }

  /**
   * Redirect to the edit sheet page
   */
  onSeeSheets() {

  }
}
