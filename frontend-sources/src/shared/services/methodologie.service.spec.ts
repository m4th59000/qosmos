import {inject, TestBed} from '@angular/core/testing';

import { MethodologieService } from './methodologie.service';

describe('MethodologieService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [MethodologieService]
  }));

  it('should be created',  inject( [MethodologieService] , () => {
    const service: MethodologieService = TestBed.get(MethodologieService);
    expect(service).toBeTruthy();
  }));
});
