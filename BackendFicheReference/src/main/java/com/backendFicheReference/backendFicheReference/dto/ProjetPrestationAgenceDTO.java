package com.backendFicheReference.backendFicheReference.dto;

import com.backendFicheReference.backendFicheReference.model.ProjetPrestationAgence;
import com.backendFicheReference.exceptions.MethodologieNotFoundException;
import com.backendFicheReference.exceptions.ProjetPrestationAgenceBadRequest;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
public class ProjetPrestationAgenceDTO {
    private static final String FORMAT_DATE = "yyyy-MM-dd";

    private Long idProjet;
    private Long idPrestation;
    private Long idAgence;
    private Long menDays;
    private String periodeIntervention;

    public Date getDatePeriodeIntervention() throws ParseException {
        return new SimpleDateFormat(FORMAT_DATE).parse(this.periodeIntervention);
    }

   /* public ProjetPrestationAgence convert() {
        final ProjetPrestationAgence projetPrestationAgence = new ProjetPrestationAgence();

        projetPrestationAgence.setIdProjet(this.idProjet);
        projetPrestationAgence.setIdPrestation(this.idPrestation);
        projetPrestationAgence.setIdAgence(this.idAgence);
        projetPrestationAgence.setMenDays(this.menDays);

        try {
            projetPrestationAgence.setPeriodeIntervention(new SimpleDateFormat(FORMAT_DATE).parse(this.periodeIntervention));
        } catch (ParseException e) {
            //TODO : Creer une exception - code erreur 400 : 	Bad Request :   La syntaxe de la requête est erronée.
            throw ProjetPrestationAgenceBadRequest.projetPrestationAgenceBadRequest();
        }

        return projetPrestationAgence;
    }*/
}

