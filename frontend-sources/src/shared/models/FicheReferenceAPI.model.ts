export class FicheReferenceAPI {
  constructor(
    public content?: any[] ,
    public pageable?: any,
    public totalPages?: number,
    public  totalElements?: number,
    public  last?: boolean,
    public  size?: number,
    public  number?: number,
    public  sort?: any,
    public numberOfElements?: number,
    public first?: boolean,
    public empty?: boolean
  ) {}
}
