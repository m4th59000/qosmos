import {Component} from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from "../../NeoSoftBordeau2018/services/auth.service";
import {AuthenticationService} from "../../../shared/_services";

@Component({
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss'],
})
/**
 * Component class for navbar page managing
 */
export class NavbarComponent {

  /**
   * Constructor
   * @param authenticationService
   * @param router
   */
  constructor(public authenticationService: AuthenticationService,public authService: AuthService, private router: Router) { }

  /**
   * Check if the user is authenticated
   */
  isAuth(): boolean {
    return this.authService.isAuth();
  }

  /**
   * Connect to the auth service
   */
  connect() {
    this.router.navigate(['/login']);
  }

  /**
   * Disconnect from the auth service
   */
  disconnect() {
    this.authenticationService.logout();
  }
}
