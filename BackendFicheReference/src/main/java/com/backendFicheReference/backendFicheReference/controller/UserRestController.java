package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.User;
import com.backendFicheReference.backendFicheReference.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotNull;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserRestController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "users/{id}", method = RequestMethod.GET)
    public Optional<User> findUser(@PathVariable("id") Long id) {
        return userService.findUser(id);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public Page<User> listUtilisateurs(@RequestParam(name = "page", defaultValue = "0") String page
            , @RequestParam(name = "size", defaultValue = "10") String size
            , @RequestParam(name = "sortedBy", defaultValue = "id") String sortedBy
            , @RequestParam(name = "sortedIn", defaultValue = "asc") String sortedIn) {
        return userService.users(page, size, sortedBy, sortedIn);
    }

    //passer le password en clair et le return en crypter
    @RequestMapping(value = "/users/authenticate", method = RequestMethod.POST)
    public User findUser(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
    public User updateUser(@RequestBody User user, @PathVariable("id") @NotNull Long id) {
        return userService.updateUser(user, id);
    }
}
