package com.backendFicheReference.backendFicheReference.model;

import com.backendFicheReference.backendFicheReference.model.pk.ProjetPrestationAgencePk;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "PROJETPRESTATIONAGENCE")
public class ProjetPrestationAgence implements Serializable {

    // champ metier

    /**
     * Identifiant de serialisation
     */
    private static final long serialVersionUID = -3719334424021086317L;

    @EmbeddedId
    private ProjetPrestationAgencePk id = new ProjetPrestationAgencePk() ;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idProjet")
    private Projet projet;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idPrestation")
    private Prestation prestation;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idAgence")
    private Agence agence;

    private Long menDays;
    private Date periodeIntervention;

    public ProjetPrestationAgence(Projet projet, Prestation prestation, Agence agence, Long menDays, Date periodeIntervention) {
        ProjetPrestationAgencePk id =new ProjetPrestationAgencePk(projet.getId(),prestation.getId(),agence.getId());
        this.id=id;
        this.projet=projet;
        this.prestation=prestation;
        this.agence=agence;
        this.menDays =menDays;
        this.periodeIntervention = periodeIntervention;
    }

    public ProjetPrestationAgence() {
    }

    public ProjetPrestationAgencePk getId() {
        return id;
    }

    public void setId(ProjetPrestationAgencePk id) {
        this.id = id;
    }

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    public Prestation getPrestation() {
        return prestation;
    }

    public void setPrestation(Prestation prestation) {
        this.prestation = prestation;
    }

    public Agence getAgence() {
        return agence;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }

    public Long getMenDays() {
        return menDays;
    }

    public void setMenDays(Long menDays) {
        this.menDays = menDays;
    }

    public Date getPeriodeIntervention() {
        return periodeIntervention;
    }

    public void setPeriodeIntervention(Date periodeIntervention) {
        this.periodeIntervention = periodeIntervention;
    }
}