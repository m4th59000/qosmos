package com.backendFicheReference.backendFicheReference.model;

import com.backendFicheReference.backendFicheReference.model.pk.PrestationMethodologiePk;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PRESTATIONMETHODOLOGIE")
public class PrestationMethodologie implements Serializable{

    private static final long serialVersionUID = -3461285399778221493L;

    @EmbeddedId
    private PrestationMethodologiePk id= new PrestationMethodologiePk() ;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idMethodologie")
    private Methodologie methodologie;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idPrestation")
    private Prestation prestation;

    public PrestationMethodologie(Methodologie methodologie ,  Prestation prestation) {
        this.id = new PrestationMethodologiePk(methodologie.getId(),prestation.getId());
        this.methodologie = methodologie;
        this.prestation=prestation;

    }

    public PrestationMethodologie() {
    }


    public PrestationMethodologiePk getId() {
        return id;
    }

    public void setId(PrestationMethodologiePk id) {
        this.id = id;
    }

    public Methodologie getMethodologie() {
        return methodologie;
    }

    public void setMethodologie(Methodologie methodologie) {
        this.methodologie = methodologie;
    }

    public Prestation getPrestation() {
        return prestation;
    }

    public void setPrestation(Prestation prestation) {
        this.prestation = prestation;
    }
}













