import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjetPrestationAgenceComponent } from './projet-prestation-agence.component';

describe('ProjetPrestationAgenceComponent', () => {
  let component: ProjetPrestationAgenceComponent;
  let fixture: ComponentFixture<ProjetPrestationAgenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjetPrestationAgenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjetPrestationAgenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
