package com.backendFicheReference.backendFicheReference.repository;

import com.backendFicheReference.backendFicheReference.model.Methodologie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MethodologieRepository extends JpaRepository<Methodologie, Long> {
    Optional<Methodologie> findById(Long id);
    Page<Methodologie> findAll(Pageable pageable);
}