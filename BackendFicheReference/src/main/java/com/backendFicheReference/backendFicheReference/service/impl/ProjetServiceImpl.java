package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.Projet;
import com.backendFicheReference.backendFicheReference.repository.ProjetRepository;
import com.backendFicheReference.backendFicheReference.service.ProjetService;
import com.backendFicheReference.exceptions.ProjetNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProjetServiceImpl implements ProjetService {

    @Autowired
    private ProjetRepository projetRepository;

    @Override
    public Optional<Projet> findProjet(Long id) {
        final Optional<Projet> optionalProjet = this.projetRepository.findById(id);

        if (optionalProjet.isPresent()) {
            final Projet projet = optionalProjet.get();
            // convertir en ProjetDTO
            // final ProjetDTO dto = new ProjetDTO(projet);
            // final ProjetDTO dto = ProjetUtils.convert(projet, "findById");
            return projetRepository.findById(id);
        } else {
            throw ProjetNotFoundException.projetNotFound();
        }
    }

    @Override
    public Projet saveProjet(Projet projet) {
        return projetRepository.save(projet);
    }

    @Override
    public Projet updateProjet(Projet projet, Long id) {
        projet.setId(id);
        return  projetRepository.save(projet);
    }

    @Override
    public Page<Projet> projets(String page , String size, String sortedBy, String sortedIn){
        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return projetRepository.findAll(pageable);
    }

}