import {inject, TestBed} from '@angular/core/testing';

import { TechnologieService } from './technologie.service';

describe('TechnologieService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [TechnologieService]
  }));

  it('should be created',  inject( [TechnologieService] , () => {
    const service: TechnologieService = TestBed.get(TechnologieService);
    expect(service).toBeTruthy();
  }));
});

