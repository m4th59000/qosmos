import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHandler, HttpHeaders, HttpParams} from '@angular/common/http';
import {Agence} from '../models/Agence.model';
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";

const path = 'agences/';
const baseUrl = environment.endpoint;

const headers= new HttpHeaders({
    'Content-type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
@Injectable({
  providedIn: 'root'
})
export class AgenceService {
  constructor(private _http: HttpClient) {

  }
  getAgence(id: string) {
    return this._http.get<Agence>(baseUrl + path + id ,{'headers': headers});
  }
  getAgences(page: number , size: number , sortedBy: string , sortedIn: string) {
    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
      .set('page', ''+page)
      .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
        );
  }
  createAgence(agence: Agence) {
    return this._http.post<Agence>(baseUrl + path , agence , {'headers': headers});
  }
  updateAgence(agence: Agence) {
    return this._http.put<Agence>(baseUrl + path , agence , {'headers': headers});
  }
}
