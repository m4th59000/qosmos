import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatStepper} from "@angular/material/stepper";
import {TypeEngagement} from "../../../shared/models/TypeEngagement.model";
import {Projet} from "../../../shared/models/Projet.model";
import {Prestation} from "../../../shared/models/Prestation.model";
import {Marquage} from "../../../shared/models/Marquage.model";

@Component({
  selector: 'app-fiche-reference',
  templateUrl: './fiche-reference.component.html',
  styleUrls: ['./fiche-reference.component.scss']
})

export class FicheReferenceComponent implements OnInit {
  contextProjetFormGroup: FormGroup ;
  prestationFormGroup: FormGroup;
  projetPrestationAgenceFormGroup: FormGroup;
  isLinear = true;
  projet: Projet;
  prestation: Prestation;
  typeEngagement: TypeEngagement;
  marquage: Marquage;
  @ViewChild('stepper') private stepper: MatStepper;
  constructor() { }
  ngOnInit() {

  }

  recieveProjet(event: Projet) {
      this.projet = event;

      console.log('recieved projet ' + JSON.stringify(this.projet));
      if(event) {
        this.goForward();
      }
  }

  recievePrestation(event: Prestation){
    this.prestation = event;
    console.log('recieved prestation ' + JSON.stringify(this.prestation));
    if(event) {
      this.goForward();
    }
  }

  recieveContextProjetForm(event: FormGroup){
    this.contextProjetFormGroup = event;
      this.goForward();
  }

  recievePrestationFormGroup(event: FormGroup){
    this.prestationFormGroup = event;
    this.goForward();
  }

  recieveProjetPrestationAgenceFormGroup(event: FormGroup){
    this.projetPrestationAgenceFormGroup = event;
    this.goForward();
  }

  goForward(){
    try {
      if(this.contextProjetFormGroup) {
        console.log('******* CONTEXT PROJET FORM ******');
        console.log(this.contextProjetFormGroup);
      }

      if (this.prestationFormGroup) {
        console.log('******* PRESTATION FORM ******');
        console.log(this.prestationFormGroup);
      }

      if (this.projetPrestationAgenceFormGroup) {
        console.log('******* PROJET PRESTATION FORM ******');
        console.log(this.projetPrestationAgenceFormGroup);
      }
      this.stepper.next();
    } catch (e) {
      console.log('error in stepper property');
    }
  }

}
