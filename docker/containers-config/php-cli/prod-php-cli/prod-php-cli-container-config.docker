################################################################################
# Qosmos - Docker - PHP-CLI Container Configuration                            #
# Maintainer: Neo-Soft                                                         #
# @author: William Pinaud <william.pinaud@neo-soft.fr>                         #
################################################################################

# Using official PHP-CLI image from docker hub
# @Uses Debian Stretch
FROM php:7.3.5-cli-stretch

# Update shell variables and add some shortcuts for future in-container uses
COPY /docker/containers-config/php-cli/prod-php-cli/.bashrc /home/qosmosphpcli/.bashrc

# Adding custom sysadmin utilities
RUN apt-get update && apt-get install -y vim nano procps net-tools wget curl iputils-ping

# Adding custom utilities for PHP-CLI containers
RUN apt-get install -y git zlib1g-dev libzip-dev unzip

# This php.ini has edits for batch processing and lesser time/memory limits
# @SEE prod-php-cli.ini
COPY /docker/containers-config/php-cli/prod-php-cli/prod-php-cli.ini /usr/local/etc/php/php.ini

# Install the additional PHP extensions required by the backend
# These extensions are installed via a special script provided by the docker hub image
RUN /usr/local/bin/docker-php-ext-install "pdo_mysql"
RUN /usr/local/bin/docker-php-ext-install "zip"

# Install composer globally (#> composer ...) to use it faster from the CLI container
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"

# Create a user to run PHP commands as non-root
# This user will have a different home directory, not a default value, but a custom one.
RUN useradd -m -s /bin/bash -d /qosmosphpcli qosmosphpcli \
    && chmod 755 /qosmosphpcli \
    && chown -R qosmosphpcli /qosmosphpcli \
    && mkdir /projects \
    && mkdir /projects/qosmos \
    && mkdir /projects/qosmos/backend
COPY /docker/containers-config/php-cli/prod-php-cli/.bashrc /qosmosphpcli/.bashrc

# Give it back to our local user
RUN chown -R qosmosphpcli /projects/qosmos


# *******************************************************************
# Then execute the minimum setup so as to have a built Symfony app...
# We do only recreate it from stratch in dev environment, watch out.
# *******************************************************************

# First, let's copy the source files.
COPY /backend/ /projects/qosmos/backend
COPY /environment-files/.env /environment-files/.env.prod /projects/qosmos/environment-files/

# DON'T synchronize via volumes any local files/dirs like var or vendor.
WORKDIR /projects/qosmos/backend/

# Recreate var/ directory if needed
RUN mkdir -pv var && chmod 755 var/ && chown -R qosmosphpcli var/

# We do exectue this as non-root. "Composer, PHP, Symfony, and general Security like that."
# This also becomes your default user when docker execing into it.
USER qosmosphpcli

# Install from the versioned, faster and prepared composer.lock
#
# --ignore-platform-reqs // Ignore php, hhvm, lib-* and ext-* requirements
#                           and force the installation even if the local machine does not fulfill these.
# --prefer-dist // Composer will install from dist instead of sources if possible. Faster.
# --no-dev // Skip installing packages listed in require-dev.
# --no-scripts // Skips execution of scripts defined in composer.json.
# --no-progress // Removes the progress display that can mess with some terminals
#                  or scripts which don't handle backspace characters.
# --no-suggest // Skips suggested packages in the output.
# --classmap-authoritative // Autoload classes from the classmap only. Implicitly enables --optimize-autoloader.
# --no-interaction // Do not ask any interactive question. Essential for Docker.
# -vvv // Ultra verbose mode. Just so you don't go to sleep or think the start has crashed.
#         Usually doesn't break terminals. If it does, remove that option, it's only better for development.
RUN composer install --ignore-platform-reqs --prefer-dist --no-dev --no-scripts --no-progress --no-suggest --classmap-authoritative --no-interaction -vvv

# Fake non-terminated command, used to keep the container alive
CMD tail -f /dev/null
