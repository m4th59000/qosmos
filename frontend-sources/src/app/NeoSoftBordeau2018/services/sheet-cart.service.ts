import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SheetCartService {

  //List for selected sheet, in order to download many sheets
  selectedSheetIdList: number[] = [];
  
  constructor() { }

  /**
   * Add a new sheet id in the selected sheet id list
   * @param id The sheet id to add
   */
  addSheetToselectedList(id: number): boolean {
    if(this.selectedSheetIdList.indexOf(id) === -1) {
      this.selectedSheetIdList.push(id);
      return true;
    } else {
      return false;
    }
  }

  /**
   * remove a sheet id from the selected sheet id list
   * @param id  the sheet id to remove
   */

  removeSheetFromselectedList(id: number): boolean {    
    if(this.selectedSheetIdList.indexOf(id) !== -1) {      
      this.selectedSheetIdList.splice(this.selectedSheetIdList.indexOf(id), 1);
      return true;
    } else {
      return false;
    }
  }
}
