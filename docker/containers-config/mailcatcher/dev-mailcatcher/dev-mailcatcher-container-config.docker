################################################################################
# Qosmos - Docker - Development Mailcatcher Container Configuration            #
# Maintainer: Neo-Soft                                                         #
# @author: William Pinaud <william.pinaud@neo-soft.fr>                         #
# This container should not be used in a production environment                #
################################################################################

# Uses alpine image
FROM ruby:2.4-alpine3.6

# Have commands stop upon any error encountered and output the commands names in the console
RUN set -xe \
    && apk add --no-cache \
        libstdc++ \
        sqlite-libs \
    && apk add --no-cache --virtual .build-deps \
        build-base \
        sqlite-dev \
    && gem install mailcatcher -v 0.6.5 --no-ri --no-rdoc \
    && apk del .build-deps

# SMTP port
EXPOSE 10721

# Webserver port (the GUI to read all outbound emails)
EXPOSE 10722

# Update shell variables and add some shortcuts for future in-container uses
COPY .bashrc /root/.bashrc

# Adding custom sysadmin utilities
RUN apt-get update && apt-get install -y vim nano procps net-tools wget curl iputils-ping

# Main command: start MailCatcher and have it listen to SMTP entries on a specific port.
CMD mailcatcher --no-quit --foreground --ip=0.0.0.0 --smtp-port=10721 --http-port=10722 && tail -f /dev/null