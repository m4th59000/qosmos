import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextProjetComponent } from './context-projet.component';

describe('ContextProjetComponent', () => {
  let component: ContextProjetComponent;
  let fixture: ComponentFixture<ContextProjetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextProjetComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
