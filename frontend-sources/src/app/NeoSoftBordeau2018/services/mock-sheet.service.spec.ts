import { TestBed } from '@angular/core/testing';

import { MockSheetService } from './mock-sheet.service';

describe('MockSheetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockSheetService = TestBed.get(MockSheetService);
    expect(service).toBeTruthy();
  });
});
