import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichageFicheReferenceComponent } from './affichageFicheReference.component';

describe('AffichageFicheReferenceComponent', () => {
  let component: AffichageFicheReferenceComponent;
  let fixture: ComponentFixture<AffichageFicheReferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichageFicheReferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichageFicheReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
