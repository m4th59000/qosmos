package com.backendFicheReference.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class AgenceNotFoundException extends RuntimeException {

    /**
     * Identifiant de serialisation
     */


    private static final String AGENCE_NON_TROUVEE = "Agence non trouvée";
    private static final String AGENCE_SUPPRIMEE = "Votre agence a été supprimée";

    public AgenceNotFoundException(String message) {
        super(message);
    }

    public static AgenceNotFoundException agenceNotFound() {
        return new AgenceNotFoundException(AGENCE_NON_TROUVEE);
    }

    public static AgenceNotFoundException agenceSupprimee() {
        return new AgenceNotFoundException(AGENCE_SUPPRIMEE);
    }
}

