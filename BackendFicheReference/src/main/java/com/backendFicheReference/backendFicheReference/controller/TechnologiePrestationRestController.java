package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.TechnologiePrestation;
import com.backendFicheReference.backendFicheReference.model.pk.TechnologiePrestationPk;
import com.backendFicheReference.backendFicheReference.service.TechnologiePrestationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TechnologiePrestationRestController {

    @Autowired
    private TechnologiePrestationService technologiePrestationService;
    @RequestMapping(value = "technologiePrestations/{idTechnologie}/{idPrestation}", method = RequestMethod.GET)
    public Optional<TechnologiePrestation> findTechnologiePrestation(@PathVariable("idTechnologie") @NotNull Long idTechnologie,
                                                                     @PathVariable("idPrestation") @NotNull Long idPrestation
    ){
        return technologiePrestationService.findTechnologiePrestation(new TechnologiePrestationPk(idTechnologie,idPrestation) );
    }

    @RequestMapping(value = "technologiePrestations/{idprestation}", method = RequestMethod.GET)
    public List<TechnologiePrestation> findPrestationTechnologie(@PathVariable("idprestation") Long idprestation
                                                                 ){
        return technologiePrestationService.findByPrestation(idprestation);
    }

    @RequestMapping(value = "/technologiePrestations", method = RequestMethod.GET)
    public Page<TechnologiePrestation> listTechnologiePrestations(@RequestParam(name = "page" , defaultValue = "0") String page
            , @RequestParam(name = "size" , defaultValue = "10")  String size
            , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
            , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){
        return technologiePrestationService.technologiePrestations(page , size,sortedBy , sortedIn);
    }

    @RequestMapping(value = "/technologiePrestations", method = RequestMethod.POST)
    public TechnologiePrestation findTechnologiePrestation(@RequestBody TechnologiePrestation technologiePrestation){
        return technologiePrestationService.saveTechnologiePrestation(technologiePrestation);
    }

    @RequestMapping(value = "/technologiePrestations/{idTechnologie}/{idPrestation}", method = RequestMethod.PUT)
    public TechnologiePrestation updateTechnologiePrestation(@RequestBody TechnologiePrestation technologiePrestation,
                                                               @PathVariable("idTechnologie") @NotNull Long idTechnologie,
                                                               @PathVariable("idPrestation") @NotNull Long idPrestation
    ){
        return technologiePrestationService.updateTechnologiePrestation(technologiePrestation, new TechnologiePrestationPk(idTechnologie,idPrestation));
    }


}





