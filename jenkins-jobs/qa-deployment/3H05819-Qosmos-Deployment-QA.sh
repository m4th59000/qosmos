cd
cp -Rfv jenkins/workspace/3H05819-Qosmos-Deployment-QA/* qosmos-qa/
cd qosmos-qa
docker-compose -f docker/global-docker-compose.yml -f docker/qa-docker-compose.yml -p "qosmos-qa" up -d --build --force-recreate