package com.backendFicheReference.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PrestationMethodologieNotFoundException extends RuntimeException {

    private static final String PRESTATIONMETHODOLOGIE_NON_TROUVEE = "PrestationMethodologie non trouvée";
    private static final String PRESTATIONMETHODOLOGIE_SUPPRIMEE = "Votre TechnologiePrestation a été supprimée";

    public PrestationMethodologieNotFoundException(String message) {
        super(message);
    }

    public static PrestationMethodologieNotFoundException prestationMethodologieNotFound() {
        return new PrestationMethodologieNotFoundException(PRESTATIONMETHODOLOGIE_NON_TROUVEE);
    }

    public static PrestationMethodologieNotFoundException prestationMethodologieSupprimee() {
        return new PrestationMethodologieNotFoundException(PRESTATIONMETHODOLOGIE_NON_TROUVEE);
    }
}










