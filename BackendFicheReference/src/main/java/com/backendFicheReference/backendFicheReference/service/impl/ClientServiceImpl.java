package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.Client;
import com.backendFicheReference.backendFicheReference.repository.ClientRepository;
import com.backendFicheReference.backendFicheReference.service.ClientService;
import com.backendFicheReference.exceptions.ClientNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Client findClient(Long id) {
        final Optional<Client> optionalClient = this.clientRepository.findById(id);

        if (optionalClient.isPresent()) {
            final Client client = optionalClient.get();
            // convertir en ClientDTO
            // final ClientDTO dto = new ClientDTO(client);
            // final ClientDTO dto = ClientUtils.convert(client, "findById");
            return client;
        } else {
            throw ClientNotFoundException.clientNotFound();
        }
    }

    @Override
    public Client saveClient(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public Client updateClient(Client client, Long id) {
        client.setId(id);
        return  clientRepository.save(client);
    }


    @Override
    public Page<Client> clients(String page , String size, String sortedBy, String sortedIn){

        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return clientRepository.findAll(pageable);
    }

}