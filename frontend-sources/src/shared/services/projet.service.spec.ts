import {inject, TestBed} from '@angular/core/testing';

import { ProjetService } from './projet.service';

describe('ProjetService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ProjetService]
  }));

  it('should be created', inject( [ProjetService] , () => {
    const service: ProjetService = TestBed.get(ProjetService);
    expect(service).toBeTruthy();
  }));
});


