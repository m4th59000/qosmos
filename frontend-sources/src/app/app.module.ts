import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {HomeComponent} from './components/home/home.component';
import {StepsComponent} from './NeoSoftBordeau2018/components/sheet/form/steps/steps.component';
import {PageNotFoundComponent} from './components/pagenotfound/page-not-found.component';
import {ReferentielComponent} from './NeoSoftBordeau2018/components/sheet/referentiel/referentiel.component';
import {StepGlobalComponent} from './NeoSoftBordeau2018/components/sheet/form/step-global/step-global.component';
import {StepMissionComponent} from './NeoSoftBordeau2018/components/sheet/form/step-mission/step-mission.component';
import {StepAdditionalComponent} from './NeoSoftBordeau2018/components/sheet/form/step-additional/step-additional.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DetailsComponent } from './NeoSoftBordeau2018/components/sheet/referentiel/details/details.component';
import { DataTablesModule } from 'angular-datatables';
import { MockSheetService } from './NeoSoftBordeau2018/services/mock-sheet.service';
import { GenericSheetService } from './NeoSoftBordeau2018/services/generic-sheet-service';
import { LoginComponent } from './components/login/login.component';
import { SheetCartService } from './NeoSoftBordeau2018/services/sheet-cart.service';
import { SheetCartComponent } from './NeoSoftBordeau2018/components/sheet/sheet-cart/sheet-cart.component';
import {A11yModule} from '@angular/cdk/a11y';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import {FicheReferenceComponent} from "./containers/fiche-reference/fiche-reference.component";
import { ContextProjetComponent } from './components/ficheReference/context-projet/context-projet.component';
import { PrestationComponent } from './components/ficheReference/prestation/prestation.component';
import { ProjetPrestationAgenceComponent } from './components/ficheReference/projet-prestation-agence/projet-prestation-agence.component';
import { FicheReferenceListComponent } from './containers/fiche-reference-list/fiche-reference-list.component';
import { AffichageFicheReferenceComponent } from './components/affichageFicheReference/affichageFicheReference.component';
import {MatIconModule} from "@angular/material/icon";
import { DocumentEditorModule } from '@txtextcontrol/tx-ng-document-editor';
import {AlertModule} from "ngx-bootstrap/alert";
import {RegisterComponent} from "./components/register";
import {AlertComponent} from "./_components";

@NgModule({
    imports: [
        MatIconModule,
        AppRoutingModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        DataTablesModule,
        A11yModule,
        ClipboardModule,
        CdkStepperModule,
        CdkTableModule,
        CdkTreeModule,
        DragDropModule,
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatStepperModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTreeModule,
        PortalModule,
        ScrollingModule,
        MatIconModule,
        DocumentEditorModule,
        AlertModule
    ],
  exports: [
    A11yModule,
    ClipboardModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
    ScrollingModule
  ],
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    HomeComponent,
    ReferentielComponent,
    StepsComponent,
    StepGlobalComponent,
    StepMissionComponent,
    StepAdditionalComponent,
    PageNotFoundComponent,
    DetailsComponent,
    LoginComponent,
    SheetCartComponent,
    FicheReferenceComponent,
    ContextProjetComponent,
    PrestationComponent,
    ProjetPrestationAgenceComponent,
    FicheReferenceListComponent,
    AffichageFicheReferenceComponent,
    RegisterComponent,
    AlertComponent
  ],
  providers: [
    {
      provide: GenericSheetService,
      useClass: MockSheetService,
    },
    SheetCartService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
