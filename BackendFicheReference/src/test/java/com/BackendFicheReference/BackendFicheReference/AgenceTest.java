package com.BackendFicheReference.BackendFicheReference;

import com.backendFicheReference.backendFicheReference.model.Agence;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AgenceTest {

    @Test
    public void agenceShouldBePresent(){

//Arrange
        Agence agence = new Agence();
//Act
        agence.setAgence("paris");
//Assert
        assertThat(agence.getAgence().equals("paris"));
    }
}
