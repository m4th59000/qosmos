package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.ProjetPrestationAgence;
import com.backendFicheReference.backendFicheReference.model.pk.ProjetPrestationAgencePk;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface ProjetPrestationAgenceService {
    Optional<ProjetPrestationAgence> findProjetPrestationAgence(ProjetPrestationAgencePk id);
    ProjetPrestationAgence saveProjetPrestationAgence(ProjetPrestationAgence projetPrestationAgence );
    ProjetPrestationAgence updateProjetPrestationAgence(ProjetPrestationAgence projetPrestationAgence , ProjetPrestationAgencePk projetPrestationAgencePk);
    Page<ProjetPrestationAgence> projetPrestationAgences(String page , String size, String sortedBy, String sortedIn);
}
