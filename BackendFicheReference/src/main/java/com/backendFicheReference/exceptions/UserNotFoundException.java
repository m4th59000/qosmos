package com.backendFicheReference.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

    /**
     * Identifiant de serialisation
     */


    private static final String USER_NON_TROUVEE = "utilisateur non trouvée";
    private static final String USER_SUPPRIMEE = "Votre utilisateur a été supprimée";

    public UserNotFoundException(String message) {
        super(message);
    }

    public static UserNotFoundException userNotFound() {
        return new UserNotFoundException(USER_NON_TROUVEE);
    }

    public static UserNotFoundException userSupprime() {
        return new UserNotFoundException(USER_SUPPRIMEE);
    }
}

