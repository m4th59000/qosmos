import {inject, TestBed} from '@angular/core/testing';

import { ProjetPrestationAgenceService } from './projet-prestation-agence.service';

describe('ProjetPrestationAgenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ProjetPrestationAgenceService]
  }));

  it('should be created', inject( [ProjetPrestationAgenceService] , () => {
    const service: ProjetPrestationAgenceService = TestBed.get(ProjetPrestationAgenceService);
    expect(service).toBeTruthy();
  }));
});
