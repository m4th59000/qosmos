package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.PrestationMethodologie;
import com.backendFicheReference.backendFicheReference.model.pk.PrestationMethodologiePk;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface PrestationMethodologieService {
    Optional<PrestationMethodologie> findPrestationMethodologie(PrestationMethodologiePk id);
    PrestationMethodologie savePrestationMethodologie(PrestationMethodologie prestationMethodologie );
    PrestationMethodologie updatePrestationMethodologie(PrestationMethodologie prestationMethodologie , PrestationMethodologiePk prestationMethodologiePk);
    Page<PrestationMethodologie> prestationMethodologies(String page , String size, String sortedBy, String sortedIn);
    List<PrestationMethodologie> findByPrestation(Long idprestation);
}









