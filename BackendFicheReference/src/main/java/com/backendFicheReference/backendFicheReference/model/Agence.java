package com.backendFicheReference.backendFicheReference.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "AGENCE")


public class Agence implements Serializable {

    // champ metier

    /**
     * Identifiant de serialisation
     */
    private static final long serialVersionUID = -5037933993683168259L;

    @Id
    @GeneratedValue(generator = "SEQ_AGENCE_ID", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String agence;

    public Agence() {
    }

    public Agence(Long id , String agence, User user) {
        this.id = id;
        this.agence = agence;

    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}