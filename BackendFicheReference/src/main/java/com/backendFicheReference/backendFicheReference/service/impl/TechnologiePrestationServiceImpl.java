package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.TechnologiePrestation;
import com.backendFicheReference.backendFicheReference.model.pk.TechnologiePrestationPk;
import com.backendFicheReference.backendFicheReference.repository.TechnologiePrestationRepository;
import com.backendFicheReference.backendFicheReference.service.TechnologiePrestationService;
import com.backendFicheReference.exceptions.TechnologiePrestationNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TechnologiePrestationServiceImpl implements TechnologiePrestationService {

    @Autowired
    private TechnologiePrestationRepository technologiePrestationRepository;

    @Override
    public Optional<TechnologiePrestation> findTechnologiePrestation(TechnologiePrestationPk id) {
        final Optional<TechnologiePrestation> optionalTechnologiePrestation = this.technologiePrestationRepository.findById(id);

        if (optionalTechnologiePrestation.isPresent()) {
            final TechnologiePrestation technologiePrestation = optionalTechnologiePrestation.get();

            return technologiePrestationRepository.findById(id);
        } else {
            throw TechnologiePrestationNotFoundException.technologiePrestationNotFound();
        }
    }

    @Override
    public TechnologiePrestation saveTechnologiePrestation(TechnologiePrestation technologiePrestation) {
        return technologiePrestationRepository.save(technologiePrestation);
    }

    @Override
    public TechnologiePrestation updateTechnologiePrestation(TechnologiePrestation technologiePrestation , TechnologiePrestationPk technologiePrestationPk) {
        technologiePrestation.setId(technologiePrestationPk);
        return technologiePrestationRepository.save(technologiePrestation);
    }

    @Override
    public Page<TechnologiePrestation> technologiePrestations(String page , String size, String sortedBy, String sortedIn){
        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return technologiePrestationRepository.findAll(pageable);
    }

    @Override
    public List<TechnologiePrestation> findByPrestation(Long prestation) {
        return technologiePrestationRepository.findByPrestationId(prestation);
    }
}














