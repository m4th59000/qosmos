
export class Technologie {
  constructor(
    public id?: number,
    public technologie?: string,
    public version?: number,
    public imageTechnologie?: string
  ) {}
}
