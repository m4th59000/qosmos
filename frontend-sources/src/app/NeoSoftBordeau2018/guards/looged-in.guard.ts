import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
/**
 * Guard for logged in
 */
export class LoogedInGuard implements CanActivate {

  /**
   * Constructor
   * @param router 
   * @param authService 
   */
  constructor(private router: Router, private authService: AuthService) {}

  /**
   * activate the route if the user is authenticated
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      const loggedIn = this.authService.isAuth();
      if(!loggedIn) {
        this.router.navigate(['/login']);
      }
      return loggedIn;
  }
}
