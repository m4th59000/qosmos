import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {TechnologiePrestation} from "../models/TechnologiePrestation.model";
import {environment} from "../../environments/environment";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";
import {PrestationMethodologie} from "../models/PrestationMethodologie.model";

const baseUrl = environment.endpoint;
const path = 'technologiePrestations/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class TechnologiePrestationService {

  constructor(private _http: HttpClient) { }

  getTechnologiePrestation(idTechnologie: string, idPrestation: string) {
    return this._http.get<TechnologiePrestation>(baseUrl + path + idTechnologie + '/' + idPrestation  , {'headers': headers});
  }
  getTechnologiePrestations(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  getByTechnologie(idtechnologie: number){
    return this._http.get<TechnologiePrestation[]>(baseUrl + path + '/' + idtechnologie, {'headers': headers});
  }

  getByPrestation(idprestation: number){
    return this._http.get<TechnologiePrestation[]>(baseUrl + path + '/' + idprestation  , {'headers': headers});
  }

  createTechnologiePrestation(technologiePrestation: TechnologiePrestation) {
    return this._http.post<TechnologiePrestation>(baseUrl + path , technologiePrestation , {'headers': headers});
  }
  updateTechnologiePrestation(technologiePrestation: TechnologiePrestation) {
    return this._http.put<TechnologiePrestation>(baseUrl + path , technologiePrestation , {'headers': headers});
  }
}
