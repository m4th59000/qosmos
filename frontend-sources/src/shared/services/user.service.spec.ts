import {inject, TestBed} from '@angular/core/testing';

import { UserService } from './user.service';

describe('UtilisateurService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [UserService]
  }));

  it('should be created',  inject( [UserService] , () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  }));
});

