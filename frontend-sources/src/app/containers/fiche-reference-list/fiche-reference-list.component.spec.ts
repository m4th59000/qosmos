import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheReferenceListComponent } from './fiche-reference-list.component';

describe('FicheReferenceListComponent', () => {
  let component: FicheReferenceListComponent;
  let fixture: ComponentFixture<FicheReferenceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheReferenceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheReferenceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
