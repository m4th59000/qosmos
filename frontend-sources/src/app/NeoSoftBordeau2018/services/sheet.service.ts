import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import {Sheet} from '../models/sheet.model';
import { HttpClient } from '@angular/common/http';
import { GenericSheetService } from './generic-sheet-service';

/**
 * Service to get server sheet list and push new sheet to server
 */
@Injectable()
export class SheetService implements GenericSheetService { 
  //The current sheet to complete and push
  sheet = new Sheet(-1);
  //Behavior subject of the current sheet
  private sheetSource = new BehaviorSubject(this.sheet);
  //Subject of the current sheet
  currentSheet = this.sheetSource.asObservable(); 
  
  //The sheet list
  sheets: Sheet[] = [];
  //Behavior subject of the sheet list
  private allSheetsSource = new BehaviorSubject(this.sheets);
  //Subject of the current sheet list
  allSheets = this.allSheetsSource.asObservable();

  //Additional fields added by the user in the forms
  additionalAgences = [];
  additionalClients = [];
  additionalPostes = [];
  additionalActivite = [];
  additionalMethode = [];
  additionalTechnologie = [];

  /**
   * Constructor
   * @param httpClient 
   */
  constructor(private httpClient: HttpClient) {
  }

  /**
   * Update the current sheet list subject
   */
  private emitSheets() {
    this.allSheetsSource.next(this.sheets);
  }

  /**
   * Get sheet list from the server
   */
  getAllSheets(): Sheet[] {
    return this.getAnyFromHttpGet('link');
  }

  /**
   * Get the sheet identified by it's id from the server
   * @param id The sheet id
   */
  getSingleSheet(id: number): Sheet {
    return this.getAnyFromHttpGet('link');
  }

  /**
   * Get agence list from the server
   */
  getAllAgences(): string[] {
    return this.getAnyFromHttpGet('link');
  }

  /**
   * Get client list from the server
   */
  getAllClients(): string[] {
    return this.getAnyFromHttpGet('link');
  }

  /**
   * Get position list from the server
   */
  getAllPostes(): string[] {
    return this.getAnyFromHttpGet('link');
  }

  /**
   * Get activity list from the server
   */
  getAllActivites(): string[] {
    return this.getAnyFromHttpGet('link');
  }

  /**
   * Get method list from the server
   */
  getAllMethods(): string[] {
    return this.getAnyFromHttpGet('link');
  }

  /**
   * Get technology list from the server
   */
  getAllTechnologies(): string[] {
    return this.getAnyFromHttpGet('link');
  }

  /**
   * Get sheet file by it's id from the server
   * @param id The sheet id
   */
  getFile(id: number) {
    throw new Error("Method not implemented.");
  }

  /**
   * Add a sheet to the server
   * @param sheet The sheet to send
   */
  addSheet(sheet: Sheet) {
    throw new Error("Method not implemented.");
  }

  /**
   * Perform a http get request and return the result
   * @param link The http link
   */
  private getAnyFromHttpGet(link: string): any {
    this.httpClient
      .get<any>(link)
      .subscribe(
        (response) => {
          return response;
        },
        (error) => {
          console.log('Erreur : ' + error);
        }
      );
    return undefined;
  }
}
