package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.Agence;
import com.backendFicheReference.backendFicheReference.repository.AgenceRepository;
import com.backendFicheReference.backendFicheReference.service.AgenceService;
import com.backendFicheReference.exceptions.AgenceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AgenceServiceImpl implements AgenceService {

    @Autowired
    private AgenceRepository agenceRepository;

    @Override
    public Optional<Agence> findAgence(Long id) {
        final Optional<Agence> optionalAgence = this.agenceRepository.findById(id);

        if (optionalAgence.isPresent()) {
            final Agence agence = optionalAgence.get();
            // convertir en AgenceDTO
            // final AgenceDTO dto = new AgenceDTO(agence);
            // final AgenceDTO dto = AgenceUtils.convert(agence, "findById");
            return agenceRepository.findById(id);
        } else {
            throw AgenceNotFoundException.agenceNotFound();
        }
    }

    @Override
    public Agence saveAgence(Agence agence) {
        return agenceRepository.save(agence);
    }

    @Override
    public Agence updateAgence(Agence agence, Long id) {
        agence.setId(id);
        return  agenceRepository.save(agence);
    }

    @Override
    public Page<Agence> agences(String page , String size, String sortedBy, String sortedIn){
        Pageable pageable ;
        if(sortedIn.equals("asc")) {
             pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
             pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return agenceRepository.findAll(pageable);
    }

}