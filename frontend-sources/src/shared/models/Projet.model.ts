import {Client} from "./Client.model";

export class Projet {
  constructor(
    public id?: number,
    public nomProjet?: string,
    public description?: string,
    public facteurCleSucces?: string,
    public codeCEGID?: number,
    public etp?: string,
    public marquage?: string,
    public client?: Client
  ) {}
}
