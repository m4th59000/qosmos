package com.backendFicheReference.backendFicheReference.repository;


import com.backendFicheReference.backendFicheReference.model.Agence;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface AgenceRepository extends JpaRepository<Agence, Long> {
   Optional<Agence> findById(Long id);
   Page<Agence> findAll(Pageable pageable);
}