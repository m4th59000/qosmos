
export class Client {

  constructor(
    public id?: number,
    public societe?: string,
    public secteurActivite?: string,
    public imageClient?: string,
  ) {}
}
