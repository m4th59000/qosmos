import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Agence} from "../../../../shared/models/Agence.model";
import {AgenceService} from "../../../../shared/services/agence.service";
import {ProjetPrestationAgenceService} from "../../../../shared/services/projet-prestation-agence.service";
import {Projet} from "../../../../shared/models/Projet.model";
import {Prestation} from "../../../../shared/models/Prestation.model";

@Component({
  selector: 'app-projet-prestation-agence',
  templateUrl: './projet-prestation-agence.component.html',
  styleUrls: ['./projet-prestation-agence.component.css']
})
export class ProjetPrestationAgenceComponent implements OnInit {
  projetPrestationAgence: FormGroup;
  selectedAgenceValue: Agence;
  agences: Agence[];
  @Input() receivedContextProjetProjet: Projet;
  @Input() receivedPrestationPrestation: Prestation;
  @Output() projetPrestationAgenceEvent = new EventEmitter<FormGroup>();
  constructor(private _formBuilder: FormBuilder , private _agenceService: AgenceService , private _projetPrestationAgenceService: ProjetPrestationAgenceService ) { }

  ngOnInit(): void {
    console.log('coucou, je suis la');
    this.buildProjetPrestationAgence();
    this.loadAgences();
    console.log('prooojet ' + Projet);
  }

  buildProjetPrestationAgence() {
    this.projetPrestationAgence = this._formBuilder.group({
      projet : ['', Validators.required],
      agence : ['', Validators.required],
      prestation : ['', Validators.required],
      menDays : ['', Validators.required],
      periodeIntervention : ['', Validators.required]
    });
    this.projetPrestationAgenceEvent.emit(this.projetPrestationAgence);
  }

  loadAgences() {
    this._agenceService.getAgences(0,100,'id', 'asc').subscribe(
      agences => {
        console.log('agences loaded');
        this.agences = agences.content;
      },
      err => {
        console.log('error while loading agences');
        this.agences = [];
      }
    );
  }

  createProjetPrestationAgence(){
    console.log('valuees');
    this.projetPrestationAgence.controls['projet'].setValue(this.receivedContextProjetProjet);
    this.projetPrestationAgence.controls['prestation'].setValue(this.receivedPrestationPrestation);
    console.log(this.projetPrestationAgence.getRawValue());
    this._projetPrestationAgenceService.createProjetPrestationAgence(this.projetPrestationAgence.getRawValue()).subscribe(
      ppa => {

        console.log('projet prestation agence bien cree');
        this.projetPrestationAgenceEvent.emit(this.projetPrestationAgence);
      },
      err => {
        console.log('error while creating ppa');
      }
    );
  }
}
