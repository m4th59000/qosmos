import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Sheet } from 'src/app/NeoSoftBordeau2018/models/sheet.model';
import { MockSheetService } from 'src/app/NeoSoftBordeau2018/services/mock-sheet.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
/**
 * Component class for sheet details page managing
 */
export class DetailsComponent implements OnInit {
  //The current sheet object
  sheet: Sheet;

  /**
   * Constructor
   * @param route
   * @param sheetService
   * @param router
   */
  constructor(private route: ActivatedRoute,
              private sheetService: MockSheetService,
              private router: Router) { }

  /**
   * Initialisation of the component
   */
  ngOnInit() {
    this.sheet = this.sheetService.getSingleSheet(this.route.snapshot.params['id']);
  }

  /**
   * Redirect to the referentiel page
   */
  onGoBack() {
    this.router.navigate(['/referentiel']);
  }

  /**
   * Redirect to the edit sheet page
   */
  onEdit() {
    this.router.navigate(['/sheet/form'], { queryParams: { id: this.sheet.id } });
  }

  /**
   * Download the current sheet file
   */
  onDownload() {
    this.sheetService.getFile(this.sheet.id);
  }
}
