import {TypeEngagement} from './TypeEngagement.model';

export class Prestation {
  constructor(
    public id?: number,
    public activiteRealise?: string,
    public activiteSpecifique?: string,
    public activiteMetier?: string,
    public caPrestation?: number,
    public reussite?: string,
    public typeEngagement?: TypeEngagement,
  ) {}
}
