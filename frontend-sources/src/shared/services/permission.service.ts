import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Permission} from "../models/Permission.model";
import {environment} from "../../environments/environment";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";

const baseUrl = environment.endpoint;
const path = 'permissions/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor(private _http: HttpClient) { }
  getPermission(id: string) {
    return this._http.get<Permission>(baseUrl + path + id , {'headers': headers});
  }
  getPermissions(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  createPermission(permission: Permission) {
    return this._http.post<Permission>(baseUrl + path , permission , {'headers': headers});
  }
  updatePermission(permission: Permission) {
    return this._http.put<Permission>(baseUrl + path , permission , {'headers': headers});
  }
}


