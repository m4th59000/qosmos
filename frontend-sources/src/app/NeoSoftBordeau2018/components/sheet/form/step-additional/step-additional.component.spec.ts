import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepAdditionalComponent } from './step-additional.component';

describe('StepAdditionalComponent', () => {
  let component: StepAdditionalComponent;
  let fixture: ComponentFixture<StepAdditionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepAdditionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepAdditionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
