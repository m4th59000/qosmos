package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.Permission;
import com.backendFicheReference.backendFicheReference.repository.PermissionRepository;
import com.backendFicheReference.backendFicheReference.service.PermissionService;
import com.backendFicheReference.exceptions.PermissionNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Override
    public Optional<Permission> findPermission(Long id) {
        final Optional<Permission> optionalPermission = this.permissionRepository.findById(id);

        if (optionalPermission.isPresent()) {
            final Permission permission = optionalPermission.get();
            // convertir en PermissionDTO
            // final PermissionDTO dto = new PermissionDTO(permission);
            // final PermissionDTO dto = PermissionUtils.convert(permission, "findById");
            return permissionRepository.findById(id);
        } else {
            throw PermissionNotFoundException.permissionNotFound();
        }
    }

    @Override
    public Permission savePermission(Permission permission) {
        return permissionRepository.save(permission);
    }

    @Override
    public Permission updatePermission(Permission permission, Long id) {
        permission.setId(id);
        return  permissionRepository.save(permission);
    }

    @Override
    public Page<Permission> permissions(String page , String size, String sortedBy, String sortedIn){

        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return permissionRepository.findAll(pageable);
    }

}