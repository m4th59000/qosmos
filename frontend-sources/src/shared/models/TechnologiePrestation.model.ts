import {Prestation} from './Prestation.model';
import {Technologie} from './Technologie.model';


export class TechnologiePrestation {
  constructor(
    public technologie?: Technologie,
    public prestation?: Prestation
  ) {}
}
