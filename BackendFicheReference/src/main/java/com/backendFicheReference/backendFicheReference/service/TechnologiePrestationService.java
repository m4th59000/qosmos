package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.TechnologiePrestation;
import com.backendFicheReference.backendFicheReference.model.pk.TechnologiePrestationPk;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface TechnologiePrestationService {
    Optional<TechnologiePrestation> findTechnologiePrestation(TechnologiePrestationPk id);
    TechnologiePrestation saveTechnologiePrestation(TechnologiePrestation technologiePrestation );
    TechnologiePrestation updateTechnologiePrestation(TechnologiePrestation technologiePrestation , TechnologiePrestationPk technologiePrestationPk);
    Page<TechnologiePrestation> technologiePrestations(String page , String size, String sortedBy, String sortedIn);
    List<TechnologiePrestation> findByPrestation(Long idprestation);
}





