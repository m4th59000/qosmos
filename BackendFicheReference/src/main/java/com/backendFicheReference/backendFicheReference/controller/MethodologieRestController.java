package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.Methodologie;
import com.backendFicheReference.backendFicheReference.service.MethodologieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MethodologieRestController{
    @Autowired
    private MethodologieService methodologieService;
    @RequestMapping(value = "methodologies/{id}", method = RequestMethod.GET)
    public Optional<Methodologie> findMethodologie(@PathVariable("id") Long id){
        return methodologieService.findMethodologie(id);
    }

    @RequestMapping(value = "/methodologies", method = RequestMethod.GET)
    public Page<Methodologie> listMethodologies(@RequestParam(name = "page" , defaultValue = "0") String page
            , @RequestParam(name = "size" , defaultValue = "10")  String size
            , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
            , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){
        return methodologieService.methodologies(page , size,sortedBy , sortedIn);
    }

    @RequestMapping(value = "/methodologies", method = RequestMethod.POST)
    public Methodologie findMethodologie(@RequestBody Methodologie methodologie){
        return methodologieService.saveMethodologie(methodologie);
    }

    @RequestMapping(value = "/methodologies/{id}", method = RequestMethod.PUT)
    public Methodologie updateMethodologie(@RequestBody Methodologie methodologie , @PathVariable("id") @NotNull Long id ){
        return methodologieService.updateMethodologie(methodologie,id);
    }
}
