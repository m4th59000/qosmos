package com.backendFicheReference.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TechnologiePrestationNotFoundException extends RuntimeException{
    private static final String TECHNOLOGIEPRESTATION_NON_TROUVEE = "TechnologiePrestation non trouvée";
    private static final String TECHNOLOGIEPRESTATION_SUPPRIMEE = "Votre TechnologiePrestation a été supprimée";

    public TechnologiePrestationNotFoundException(String message) {
        super(message);
    }

    public static TechnologiePrestationNotFoundException technologiePrestationNotFound() {
        return new TechnologiePrestationNotFoundException(TECHNOLOGIEPRESTATION_NON_TROUVEE);
    }

    public static TechnologiePrestationNotFoundException technologiePrestationSupprimee() {
        return new TechnologiePrestationNotFoundException(TECHNOLOGIEPRESTATION_NON_TROUVEE);
    }
}











