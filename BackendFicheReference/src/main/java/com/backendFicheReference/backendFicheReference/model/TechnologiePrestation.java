package com.backendFicheReference.backendFicheReference.model;

import com.backendFicheReference.backendFicheReference.model.pk.TechnologiePrestationPk;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "TECHNOLOGIEPRESTATION")
public class TechnologiePrestation implements Serializable {

    private static final long serialVersionUID = -5886750294273844438L;
    @EmbeddedId
    private TechnologiePrestationPk id = new TechnologiePrestationPk();

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idTechnologie")
    private Technologie technologie;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idPrestation")
    private Prestation prestation;

    public TechnologiePrestation(Technologie technologie , Prestation prestation) {
        this.id = new TechnologiePrestationPk(technologie.getId(),prestation.getId());
        this.prestation = prestation;
        this.technologie=technologie;
    }

    public TechnologiePrestation() {
    }

    public TechnologiePrestationPk getId() {
        return id;
    }

    public void setId(TechnologiePrestationPk id) {
        this.id = id;
    }

    public Technologie getTechnologie() {
        return technologie;
    }

    public void setTechnologie(Technologie technologie) {
        this.technologie = technologie;
    }

    public Prestation getPrestation() {
        return prestation;
    }

    public void setPrestation(Prestation prestation) {
        this.prestation = prestation;
    }
}














