import {inject, TestBed} from '@angular/core/testing';

import { PrestationService } from './prestation.service';

describe('PrestationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [PrestationService]
  }));

  it('should be created', inject( [PrestationService] , () => {
    const service: PrestationService = TestBed.get(PrestationService);
    expect(service).toBeTruthy();
  }));
});


