import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Sheet} from '../../../../models/sheet.model';
import {SheetService} from '../../../../services/sheet.service';
import { MockSheetService } from 'src/app/NeoSoftBordeau2018/services/mock-sheet.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sheet-form-step-global',
  templateUrl: 'step-global.component.html',
  styleUrls: ['step-global.component.scss']
})
/**
 * Component class for sheet form - step global page managing
 */
export class StepGlobalComponent implements OnInit {
  //Message event used to change the step of the form
  @Output() messageEvent = new EventEmitter<string>();

  //Agency list, used in the html select component 'selectAgence'
  agences = [''];
  //Client list, used in the html select component 'selectClient'
  clients = [''];
  //Position list, used in the html select component 'selectPost'
  postes = [''];

  //The current sheet to complete
  sheet: Sheet;

  newAgence: string;
  newClient: string;
  newPoste: string;

  /**
   * Constructor
   * @param sheetService
   * @param router
   */
  constructor(private sheetService: MockSheetService, private router: Router) {
  }

  /**
   * Initialisation of the component
   */
  ngOnInit() {
    //Subscribe to the current sheet service
    this.sheetService.currentSheet.subscribe(sheet => this.sheet = sheet);

    //Get the datas from the sheet service
    this.agences.push(...this.sheetService.getAllAgences());
    this.clients.push(...this.sheetService.getAllClients());
    this.postes.push(...this.sheetService.getAllPostes());
  }

  /**
   * Redirect to the step mission
   */
  goStepMission() {
    this.messageEvent.emit('goStepMission');
  }

  /**
   * Check if the form is invalid
   */
  formIsInvalid() {
    if ((this.sheet.agence !== undefined && this.sheet.agence !== '')
        && (this.sheet.client !== undefined && this.sheet.client !== '')
        && (this.sheet.poste !== undefined && this.sheet.poste !== ''))
      return false;
    else
      return true;
  }

  /**
   * Add new value in the given list
   * @param newField new value to add
   * @param array the list which wait for the new value
   */
  addNewField(newField: string, array: string[]) {
    if(this.isValid(newField) && this.isNotExist(newField, array))
    {
      array.push(newField);

      if(array == this.agences) {
        this.sheetService.additionalAgences.push(newField);
        this.sheet.agence = newField;
      } else if(array == this.clients) {
        this.sheetService.additionalClients.push(newField);
        this.sheet.client = newField;
      } else if(array == this.postes) {
        this.sheetService.additionalPostes.push(newField);
        this.sheet.poste = newField;
      }
    } else {
      console.log('already exist');
    }
  }

  /**
   * Check if the new value is valid
   * @param newField the value to check
   */
  private isValid(newField: string) {
    if(newField !== undefined && newField != '')
      return true;
    else
      return false;
  }

  /**
   * Check if the new value exist in the given list
   * @param newField the new value to check
   * @param array the list to check
   */
  private isNotExist(newField: string, array: string[]) {
    var exist = false;

    array.forEach((field) => {
      if(field.toLowerCase().includes('-')) {

        field.split('-').forEach((subfield) => {
          if(subfield.toLowerCase() == newField.toLowerCase())
            exist = true;
        });
      }
      if(field.toLowerCase() == newField.toLowerCase()) {
        exist = true;
      }
    });

    return !exist;
  }

  /**
   * Cancel the edition
   */
  onCancelEdition() {
    this.sheetService.editMode = false;
    this.router.navigate(['/sheet', 'form']);
  }

  getEditMode(): boolean {
    return this.sheetService.editMode;
  }
}
