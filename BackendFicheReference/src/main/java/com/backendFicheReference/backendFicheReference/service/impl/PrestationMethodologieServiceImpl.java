package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.PrestationMethodologie;
import com.backendFicheReference.backendFicheReference.model.pk.PrestationMethodologiePk;
import com.backendFicheReference.backendFicheReference.repository.PrestationMethodologieRepository;
import com.backendFicheReference.backendFicheReference.service.PrestationMethodologieService;
import com.backendFicheReference.exceptions.PrestationMethodologieNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PrestationMethodologieServiceImpl implements PrestationMethodologieService {

    @Autowired
    private PrestationMethodologieRepository prestationMethodologieRepository;

    @Override
    public Optional<PrestationMethodologie> findPrestationMethodologie(PrestationMethodologiePk id) {
        final Optional<PrestationMethodologie> optionalPrestationMethodologie = this.prestationMethodologieRepository.findById(id);

        if (optionalPrestationMethodologie.isPresent()) {
            final PrestationMethodologie prestationMethodologie = optionalPrestationMethodologie.get();

            return prestationMethodologieRepository.findById(id);
        } else {
            throw PrestationMethodologieNotFoundException.prestationMethodologieNotFound();
        }
    }

    @Override
    public PrestationMethodologie savePrestationMethodologie(PrestationMethodologie prestationMethodologie) {
        return prestationMethodologieRepository.save(prestationMethodologie);
    }

    @Override
    public PrestationMethodologie updatePrestationMethodologie(PrestationMethodologie prestationMethodologie , PrestationMethodologiePk prestationMethodologiePk) {
        prestationMethodologie.setId(prestationMethodologiePk);
        return prestationMethodologieRepository.save(prestationMethodologie);
    }

    @Override
    public Page<PrestationMethodologie> prestationMethodologies(String page , String size, String sortedBy, String sortedIn){
        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return prestationMethodologieRepository.findAll(pageable);
    }

    @Override
    public List<PrestationMethodologie> findByPrestation(Long idprestation) {
        return prestationMethodologieRepository.findByPrestationId(idprestation);
    }

}














