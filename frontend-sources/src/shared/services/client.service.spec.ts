import {inject, TestBed} from '@angular/core/testing';

import { ClientService } from './client.service';

describe('ClientService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ClientService]
  }));

  it('should be created', inject( [ClientService] , () => {
    const service: ClientService = TestBed.get(ClientService);
    expect(service).toBeTruthy();
  }));
});
