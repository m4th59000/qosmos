package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.Agence;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface AgenceService {
    Optional<Agence> findAgence(Long id);
    Agence saveAgence(Agence agence);
    Agence updateAgence(Agence agence , Long id);
    Page<Agence> agences(String page , String size, String sortedBy, String sortedIn);
}