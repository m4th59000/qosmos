package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.ProjetPrestationAgence;
import com.backendFicheReference.backendFicheReference.model.pk.ProjetPrestationAgencePk;
import com.backendFicheReference.backendFicheReference.service.ProjetPrestationAgenceService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProjetPrestationAgenceRestController{
    private final ProjetPrestationAgenceService projetPrestationAgenceService;

    public ProjetPrestationAgenceRestController(ProjetPrestationAgenceService projetPrestationAgenceService) {
        this.projetPrestationAgenceService = projetPrestationAgenceService;
    }

    @RequestMapping(value = "projetPrestationAgences/{idProjet}/{idPrestation}/{idAgence}", method = RequestMethod.GET)
    public Optional<ProjetPrestationAgence> findProjetPrestationAgence(@PathVariable("idProjet") @NotNull Long idProjet,
                                                                       @PathVariable("idPrestation") @NotNull Long idPrestation,
                                                                       @PathVariable("idAgence") @NotNull Long idAgence
                                                                       ){
        return projetPrestationAgenceService.findProjetPrestationAgence(new ProjetPrestationAgencePk(idProjet,idPrestation,idAgence) );
    }

    @RequestMapping(value = "/projetPrestationAgences", method = RequestMethod.GET)
    public Page<ProjetPrestationAgence> listProjetPrestationAgences(@RequestParam(name = "page" , defaultValue = "0") String page
            , @RequestParam(name = "size" , defaultValue = "10")  String size
            , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
            , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){
        return projetPrestationAgenceService.projetPrestationAgences(page , size,sortedBy , sortedIn);
    }

    @RequestMapping(value = "/projetPrestationAgences", method = RequestMethod.POST)
    public ProjetPrestationAgence findProjetPrestationAgence(@RequestBody ProjetPrestationAgence projetPrestationAgence){
        return projetPrestationAgenceService.saveProjetPrestationAgence(projetPrestationAgence);
    }

    @RequestMapping(value = "/projetPrestationAgences/{idProjet}/{idPrestation}/{idAgence}", method = RequestMethod.PUT)
    public ProjetPrestationAgence updateProjetPrestationAgence(@RequestBody ProjetPrestationAgence projetPrestationAgence,
                                                               @PathVariable("idProjet") @NotNull Long idProjet,
                                                               @PathVariable("idPrestation") @NotNull Long idPrestation,
                                                               @PathVariable("idAgence") @NotNull Long idAgence){
        return projetPrestationAgenceService.updateProjetPrestationAgence(projetPrestationAgence, new ProjetPrestationAgencePk(idProjet,idPrestation,idAgence));
    }
}
