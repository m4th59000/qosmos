package com.backendFicheReference.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ClientNotFoundException extends RuntimeException {

    /**
     * Identifiant de serialisation
     */


    private static final String CLIENT_NON_TROUVEE = "Client non trouvée";
    private static final String CLIENT_SUPPRIMEE = "Votre client a été supprimée";

    public ClientNotFoundException(String message) {
        super(message);
    }

    public static ClientNotFoundException clientNotFound() {
        return new ClientNotFoundException(CLIENT_NON_TROUVEE);
    }

    public static ClientNotFoundException clientSupprimee() {
        return new ClientNotFoundException(CLIENT_SUPPRIMEE);
    }
}

