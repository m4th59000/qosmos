import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './components/pagenotfound/page-not-found.component';
import { LoogedInGuard } from './NeoSoftBordeau2018/guards/looged-in.guard';
import { NotLoggedGuard } from './NeoSoftBordeau2018/guards/not-logged.guard';
import { LoginComponent } from './components/login/login.component';
import {FicheReferenceComponent} from "./containers/fiche-reference/fiche-reference.component";
import {FicheReferenceListComponent} from "./containers/fiche-reference-list/fiche-reference-list.component";
import {AffichageFicheReferenceComponent} from "./components/affichageFicheReference/affichageFicheReference.component";
import {HomeComponent} from "./components/home";
import {AuthGuard} from "./_helpers";
import {RegisterComponent} from "./components/register";

/**
 * Routing of the application
 */
export const routes: Routes = [
  {
    path: '', component: HomeComponent,
    canActivate: [AuthGuard] },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [LoogedInGuard]
  },
  {
    path: 'login',
    component: LoginComponent },
  {
    path: 'register',
    component: RegisterComponent },
  {
    path: 'referentiel',
    component: FicheReferenceListComponent,
    canActivate: [LoogedInGuard]
  },
  {
    path: 'sheet/form',
    component: FicheReferenceComponent,
    canActivate: [LoogedInGuard],

  },
  {
    path: 'sheets/view',
    component: AffichageFicheReferenceComponent,
    canActivate: [LoogedInGuard]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { /*enableTracing: true*/ })], // <-- TODO remove enable tracing, it's for debug
  exports: [RouterModule]
})
export class AppRoutingModule { }
