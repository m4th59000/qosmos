import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheReferenceComponent } from './fiche-reference.component';

describe('FicheReferenceComponent', () => {
  let component: FicheReferenceComponent;
  let fixture: ComponentFixture<FicheReferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheReferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
