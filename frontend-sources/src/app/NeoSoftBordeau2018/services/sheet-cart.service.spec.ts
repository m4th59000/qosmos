import { TestBed } from '@angular/core/testing';

import { SheetCartService } from './sheet-cart.service';

describe('SheetCartService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SheetCartService = TestBed.get(SheetCartService);
    expect(service).toBeTruthy();
  });
});
