import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Sheet} from '../../../../models/sheet.model';
import {SheetService} from '../../../../services/sheet.service';
import { MockSheetService } from 'src/app/NeoSoftBordeau2018/services/mock-sheet.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sheet-form-step-additional',
  templateUrl: 'step-additional.component.html',
  styleUrls: ['step-additional.component.scss']
})
/**
 * Component class for sheet form - step additional page managing
 */
export class StepAdditionalComponent implements OnInit {
  /**
   * Message event used to change the step of the form
   */
  @Output() messageEvent = new EventEmitter<string>();

  /**
   * The current sheet to complete
   */
  sheet: Sheet;

  /**
   * Constructor
   * @param sheetService
   * @param router
   */
  constructor(private sheetService: MockSheetService, private router: Router) {
  }

  /**
   * Initialisation of the component
   */
  ngOnInit() {
    this.sheetService.currentSheet.subscribe(sheet => this.sheet = sheet);

  }

  /**
   * Finalize the current sheet object with id and add it to the sheet service
   */
  createSheet() {
    var currentSheetId = this.sheet.id;
    /**
     * Set the id of the sheet if it's not in edition mode
     */
    if(!this.sheetService.editMode) {
      currentSheetId = (new Date).getTime()
      this.sheet.id = currentSheetId;
    }

    console.log(this.sheet);

    /**
     * Add the current sheet to the sheet service
     */
    this.sheetService.addSheet(this.sheet);

    /**
     * Redirect to the sheet view
     */
    this.router.navigate(['/sheets', 'view', currentSheetId]);
  }

  /**
   * Redirect to the step mission
   */
  goStepMission() {
    this.messageEvent.emit('goStepMission');
  }

  /**
   * Cancel the edition
   */
  onCancelEdition() {
    this.sheetService.editMode = false;
    this.router.navigate(['/sheet', 'form']);
  }

  getEditMode(): boolean {
    return this.sheetService.editMode;
  }
}
