package com.backendFicheReference.backendFicheReference.repository;


import com.backendFicheReference.backendFicheReference.model.TechnologiePrestation;
import com.backendFicheReference.backendFicheReference.model.pk.TechnologiePrestationPk;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TechnologiePrestationRepository extends JpaRepository<TechnologiePrestation, TechnologiePrestationPk> {
    Optional<TechnologiePrestation> findById(TechnologiePrestationPk id);
    Page<TechnologiePrestation> findAll(Pageable pageable);
    List<TechnologiePrestation> findByPrestationId(Long idprestation);
}



