package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.Prestation;
import com.backendFicheReference.backendFicheReference.repository.PrestationRepository;
import com.backendFicheReference.backendFicheReference.service.PrestationService;
import com.backendFicheReference.exceptions.PrestationNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PrestationServiceImpl implements PrestationService {

    @Autowired
    private PrestationRepository prestationRepository;

    @Override
    public Optional<Prestation> findPrestation(Long id) {
        final Optional<Prestation> optionalPrestation = this.prestationRepository.findById(id);

        if (optionalPrestation.isPresent()) {
            final Prestation prestation = optionalPrestation.get();
            // convertir en PrestationDTO
            // final PrestationDTO dto = new PrestationDTO(prestation);
            // final PrestationDTO dto = PrestationUtils.convert(prestation, "findById");
            return prestationRepository.findById(id);
        } else {
            throw PrestationNotFoundException.prestationNotFound();
        }
    }

    @Override
    public Prestation savePrestation(Prestation prestation) {
        return prestationRepository.save(prestation);
    }

    @Override
    public Prestation updatePrestation(Prestation prestation, Long id) {
        prestation.setId(id);
        return  prestationRepository.save(prestation);
    }
    
    @Override
    public Page<Prestation> prestations(String page , String size, String sortedBy, String sortedIn){
        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return prestationRepository.findAll(pageable);
    }

}