package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.Prestation;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface PrestationService {
    Optional<Prestation> findPrestation(Long id);
    Prestation savePrestation(Prestation prestation);
    Prestation updatePrestation(Prestation prestation , Long id);
    Page<Prestation> prestations(String page , String size, String sortedBy, String sortedIn);
}