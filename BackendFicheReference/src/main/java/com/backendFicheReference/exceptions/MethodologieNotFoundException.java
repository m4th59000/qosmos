package com.backendFicheReference.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MethodologieNotFoundException extends RuntimeException {

    /**
     * Identifiant de serialisation
     */


    private static final String METHODOLOGIE_NON_TROUVEE = "Methodologie non trouvée";
    private static final String METHODOLOGIE_SUPPRIMEE = "Votre methodologie a été supprimée";

    public MethodologieNotFoundException(String message) {
        super(message);
    }

    public static MethodologieNotFoundException methodologieNotFound() {
        return new MethodologieNotFoundException(METHODOLOGIE_NON_TROUVEE);
    }

    public static MethodologieNotFoundException methodologieSupprimee() {
        return new MethodologieNotFoundException(METHODOLOGIE_SUPPRIMEE);
    }
}

