import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";
import {User} from "../models/User.model";

const baseUrl = environment.endpoint;
const path = 'users/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: HttpClient) { }
  getUser(id: string) {
    return this._http.get<User>(baseUrl + path + id , {'headers': headers});
  }
  getUsers(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  createUser(user: User) {
    return this._http.post<User>(baseUrl + path , user , {'headers': headers});
  }
  updateUser(user: User) {
    return this._http.put<User>(baseUrl + path , user , {'headers': headers});
  }
  deleteUser(id: number) {
    return this._http.delete(`${config.apiUrl}/users/${id}`);
  }
}

