package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.Methodologie;
import com.backendFicheReference.backendFicheReference.repository.MethodologieRepository;
import com.backendFicheReference.backendFicheReference.service.MethodologieService;
import com.backendFicheReference.exceptions.MethodologieNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MethodologieServiceImpl implements MethodologieService {

    @Autowired
    private MethodologieRepository methodologieRepository;

    @Override
    public Optional<Methodologie> findMethodologie(Long id) {
        final Optional<Methodologie> optionalMethodologie = this.methodologieRepository.findById(id);

        if (optionalMethodologie.isPresent()) {
            final Methodologie methodologie = optionalMethodologie.get();
            // convertir en MethodologieDTO
            // final MethodologieDTO dto = new MethodologieDTO(methodologie);
            // final MethodologieDTO dto = MethodologieUtils.convert(methodologie, "findById");
            return methodologieRepository.findById(id);
        } else {
            throw MethodologieNotFoundException.methodologieNotFound();
        }
    }

    @Override
    public Methodologie saveMethodologie(Methodologie methodologie) {
        return methodologieRepository.save(methodologie);
    }

    @Override
    public Methodologie updateMethodologie(Methodologie methodologie, Long id) {
        methodologie.setId(id);
        return  methodologieRepository.save(methodologie);
    }

    @Override
    public Page<Methodologie> methodologies(String page , String size, String sortedBy, String sortedIn){
        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return methodologieRepository.findAll(pageable);
    }

}