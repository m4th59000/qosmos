package com.backendFicheReference.backendFicheReference.model;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "METHODOLOGIE")


public class Methodologie implements Serializable {

    // champ metier

    /**
     * Identifiant de serialisation
     */
    private static final long serialVersionUID = 6693759675956603591L;

    @Id
    @GeneratedValue(generator = "SEQ_METHODOLOGIE_ID", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String methodologie;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMethodologie() {
        return methodologie;
    }

    public void setMethodologie(String methodologie) {
        this.methodologie = methodologie;
    }
}