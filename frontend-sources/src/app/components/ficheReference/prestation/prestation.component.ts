import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {TypeEngagement} from "../../../../shared/models/TypeEngagement.model";
import {PrestationService} from "../../../../shared/services/prestation.service";
import {Prestation} from "../../../../shared/models/Prestation.model";
import {Technologie} from "../../../../shared/models/Technologie.model";
import {Methodologie} from "../../../../shared/models/Methodologie.model";
import {TechnologieService} from "../../../../shared/services/technologie.service";
import {MethodologieService} from "../../../../shared/services/methodologie.service";
import {TechnologiePrestationService} from "../../../../shared/services/technologie-prestation.service";
import {TechnologiePrestation} from "../../../../shared/models/TechnologiePrestation.model";
import {PrestationMethodologieService} from "../../../../shared/services/prestation-methodologie.service";
import {PrestationMethodologie} from "../../../../shared/models/PrestationMethodologie.model";

@Component({
  selector: 'app-prestation',
  templateUrl: './prestation.component.html',
  styleUrls: ['./prestation.component.css']
})
export class PrestationComponent implements OnInit {
  prestationForm: FormGroup;
  selectedTypeEngagementValue: TypeEngagement;
  listTechnologies: Technologie[];
  listMethodologies: Methodologie[];
  technologiesControl = new FormControl();
  methodologiesControl = new FormControl();
  selectedTechnologies: Technologie[];
  selectedMethodologies: Methodologie[];
  @Output() prestationEvent = new EventEmitter<Prestation>();
  @Output() prestationFormEvent = new EventEmitter<FormGroup>();
  constructor(private _formBuilder: FormBuilder ,  private _prestationService: PrestationService , private _technologieService: TechnologieService
  , private _methodologieService: MethodologieService , private _technologiePrestationService: TechnologiePrestationService ,
              private _methodologiesPrestationService: PrestationMethodologieService) { }

  ngOnInit(): void {
    this.buildPrestation();

    this.loadMethodologies();
    this.loadTechnologies();
  }

  buildPrestation() {
    this.prestationForm = this._formBuilder.group({
      activiteRealise : ['', Validators.required],
      activiteSpecifique : ['', Validators.required],
      /*activiteMetier : */
      caPrestation : ['', Validators.required],
      /*reussite : */
      typeEngagement : ['', Validators.required],
    });
    this.prestationFormEvent.emit(this.prestationForm);
  }

  createPrestation() {
    this._prestationService.createPrestation(this.prestationForm.getRawValue()).subscribe(
      prestation => {
        //send prestation and go forward
        //create prestation tech and pres meth
        console.log('selected lists  ' + JSON. stringify(this.selectedMethodologies));
        console.log('selected lists  ' + JSON. stringify(this.selectedTechnologies));
        this.createPrestationTechnologie(prestation , this.selectedTechnologies);
        this.createPrestationMethodologie(prestation , this.selectedMethodologies);
        console.log('prestation created' + JSON.stringify(prestation));
        this.prestationEvent.emit(prestation);

      },
      err => {
        console.log('error while creating prestation ' );
      }
    );
  }

  loadTechnologies(){
    this._technologieService.getTechnologieServices(0,100,'id','asc').subscribe(
      technologies => {
        this.listTechnologies = technologies.content;
      }
    );
  }

  loadMethodologies(){
    this._methodologieService.getMethodologies(0,100,'id','asc').subscribe(
      methodologies => {
        this.listMethodologies = methodologies.content;
      }
    );
  }

  createPrestationTechnologie(p: Prestation , listTech: Technologie[]) {
    for (let tech of listTech) {
      this._technologiePrestationService.createTechnologiePrestation(new TechnologiePrestation(tech , p)).subscribe(
        techPrest => {
          console.log('instance created');
        },
        err => {
          console.log('error while creating instances');
        }
      );
    }
  }
  createPrestationMethodologie(p: Prestation , listMeth: Methodologie[]) {
    for (let meth of listMeth) {
      this._methodologiesPrestationService.createPrestationMethodologie(new PrestationMethodologie(meth , p)).subscribe(
        techPrest => {
          console.log('instance created');
        },
        err => {
          console.log('error while creating instances');
        }
      );
    }
  }

}
