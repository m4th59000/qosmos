package com.backendFicheReference.backendFicheReference.model.pk;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ProjetPrestationAgencePk implements Serializable {


    private Long idProjet;
    private Long idPrestation;
    private Long idAgence;

    public ProjetPrestationAgencePk() {
    }

    public ProjetPrestationAgencePk(Long idProjet, Long idPrestation, Long idAgence) {
        this.idProjet = idProjet;
        this.idPrestation = idPrestation;
        this.idAgence = idAgence;
    }

    public Long getIdProjet() {
        return idProjet;
    }

    public void setIdProjet(Long idProjet) {
        this.idProjet = idProjet;
    }

    public Long getIdPrestation() {
        return idPrestation;
    }

    public void setIdPrestation(Long idPrestation) {
        this.idPrestation = idPrestation;
    }

    public Long getIdAgence() {
        return idAgence;
    }

    public void setIdAgence(Long idAgence) {
        this.idAgence = idAgence;
    }
}
