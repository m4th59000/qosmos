package com.backendFicheReference.backendFicheReference.service;


import com.backendFicheReference.backendFicheReference.model.User;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface UserService {
    Optional<User> findUser(Long id);

    User saveUser(User user);

    User updateUser(User user, Long id);

    Page<User> users(String page, String size, String sortedBy, String sortedIn);

}
