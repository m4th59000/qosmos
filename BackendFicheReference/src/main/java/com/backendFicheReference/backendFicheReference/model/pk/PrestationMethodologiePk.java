package com.backendFicheReference.backendFicheReference.model.pk;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class PrestationMethodologiePk implements Serializable{

    private Long idMethodologie;
    private Long idPrestation;

    public PrestationMethodologiePk() {
    }
    public PrestationMethodologiePk(Long idMethodologie, Long idPrestation) {
        this.idMethodologie = idMethodologie;
        this.idPrestation = idPrestation;
    }

    public Long getIdMethodologie() {
        return idMethodologie;
    }

    public void setIdMethodologie(Long idMethodologie) {
        this.idMethodologie = idMethodologie;
    }

    public Long getIdPrestation() {
        return idPrestation;
    }

    public void setIdPrestation(Long idPrestation) {
        this.idPrestation = idPrestation;
    }
}
