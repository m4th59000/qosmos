package com.backendFicheReference.backendFicheReference.repository;

import com.backendFicheReference.backendFicheReference.model.Technologie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface TechnologieRepository extends JpaRepository<Technologie, Long> {
    Optional<Technologie> findById(Long id);
    Page<Technologie> findAll(Pageable pageable);
}