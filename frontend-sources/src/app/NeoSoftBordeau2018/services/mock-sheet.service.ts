import { Injectable } from '@angular/core';
import { GenericSheetService } from './generic-sheet-service';
import { Sheet } from '../models/sheet.model';
import { BehaviorSubject } from 'rxjs';

/**
 * Service to get mocked sheet list and push new sheet to mock list
 */
@Injectable({
  providedIn: 'root'
})
export class MockSheetService implements GenericSheetService {

  //The current sheet to complete and push
  sheet = new Sheet(-1);
  //Behavior subject of the current sheet
  private sheetSource = new BehaviorSubject(this.sheet);
  //Subject of the current sheet
  currentSheet = this.sheetSource.asObservable();
  //Edition mode indicator, define if the sheet is edited
  editMode = false;

  //The sheet list
  sheets: Sheet[] = [];
  //Behavior subject of the sheet list
  private allSheetsSource = new BehaviorSubject(this.sheets);
  //Subject of the current sheet list
  allSheets = this.allSheetsSource.asObservable();

  //Additional fields added by the user
  additionalAgences = [];
  additionalClients = [];
  additionalPostes = [];
  additionalActivite = [];
  additionalMethode = [];
  additionalTechnologie = [];

  /**
   * Constructor
   * @param httpClient
   */
  constructor() {
    var sheet1 = new Sheet(1548340999376);
    sheet1.name = 'paris_hager_02nov2018';
    sheet1.agence = "Paris";
    sheet1.client = "Hager";
    sheet1.poste = "Analyste/Développeur";
    sheet1.activites = ['activité1', 'activité2'];
    sheet1.methodes = ['SCRUM', 'Agilité', 'Cycle en V'];
    sheet1.nombreCollaborateurs = "5";
    sheet1.technologies = ['Java 8', 'PHP 7'];
    sheet1.avantages = "Liste des avantages de la mission";
    sheet1.contextesEtEnjeux = "Le contexte est noté ici ainsi que ses enjeux au sein du projet.";
    sheet1.realisations = "La réalisation de ce projet est décrite dans ce paragraphe.";

    var sheet2 = new Sheet(1548341009327);
    sheet2.name = "aixmars_solocal_14aou2018";
    sheet2.agence = "Aix-Marseille";
    sheet2.client = "Solocal";
    sheet2.poste = "Chef de projet technique";
    sheet2.activites = ['activité55', 'activité3', 'activité5'];
    sheet2.methodes = ['Cycle en V'];
    sheet2.nombreCollaborateurs = "1";
    sheet2.technologies = ['PHP 7'];
    sheet2.avantages = "Liste des avantages de la mission";
    sheet2.contextesEtEnjeux = "Le contexte est noté ici ainsi que ses enjeux au sein du projet.";
    sheet2.realisations = "La réalisation de ce projet est décrite dans ce paragraphe.";

    var sheet3 = new Sheet(1548341017013);
    sheet3.name = "paris_solocal_22sep2018";
    sheet3.agence = "Paris";
    sheet3.client = "Solocal";
    sheet3.poste = "Chef de projet technique";
    sheet3.activites = ['activité55', 'activité3', 'activité5'];
    sheet3.methodes = ['Cycle en V'];
    sheet3.nombreCollaborateurs = "1";
    sheet3.technologies = ['PHP 7'];
    sheet3.avantages = "Liste des avantages de la mission";
    sheet3.contextesEtEnjeux = "Le contexte est noté ici ainsi que ses enjeux au sein du projet.";
    sheet3.realisations = "La réalisation de ce projet est décrite dans ce paragraphe.";

    var sheet4 = new Sheet(1548341023165);
    sheet4.name = "lille_cdisc_6jan2019";
    sheet4.agence = "Lille";
    sheet4.client = "CDiscount";
    sheet4.poste = "Concepteur fonctionnel";
    sheet4.activites = ['activité55', 'activité3', 'activité5'];
    sheet4.methodes = ['Cycle en V'];
    sheet4.nombreCollaborateurs = "1";
    sheet4.technologies = ['PHP 7'];
    sheet4.avantages = "Liste des avantages de la mission";
    sheet4.contextesEtEnjeux = "Le contexte est noté ici ainsi que ses enjeux au sein du projet.";
    sheet4.realisations = "La réalisation de ce projet est décrite dans ce paragraphe.";

    this.sheets = [
      sheet1,
      sheet2,
      sheet3,
      sheet4
    ];
    this.emitSheets();
  }

  /**
   * Update the current sheet list subject
   */
  private emitSheets() {
    this.allSheetsSource.next(this.sheets);
  }

  /**
   * Get mock sheet list
   */
  getAllSheets(): Sheet[] {
    return this.sheets;
  }

  /**
   * Get the mock sheet identified by it's id
   * @param id The sheet id
   */
  getSingleSheet(id: number): Sheet {
    var result = this.sheets.find(obj => {
      return obj.id === +id
    });
    return result;
  }

  /**
   * Get mock agence list
   */
  getAllAgences(): string[] {
    var array = ['Bordeaux-Limoges',
    'Paris',
    'Lille',
    'Nantes',
    'Aix-Marseille'];
    array.push(...this.additionalAgences);
    return array;
  }

  /**
   * Get mock client list
   */
  getAllClients(): string[] {
    var array = ['Hager', 'Solocal', 'CDiscount'];
    array.push(...this.additionalClients);
    return array;
  }

  /**
   * Get mock position list
   */
  getAllPostes(): string[] {
    var array = ['Analyste/Développeur', 'Concepteur fonctionnel', 'Chef de projet technique'];
    array.push(...this.additionalPostes);
    return array;
  }

  /**
   * Get mock activity list
   */
  getAllActivites(): string[] {
    var array = ['activité1', 'activité2', 'activité3', 'activité4'];
    array.push(...this.additionalActivite);
    return array;
  }

  /**
   * Get mock method list
   */
  getAllMethods(): string[] {
    var array = ['SCRUM', 'Agilité', 'TDD', 'DDD', 'Cycle en V'];
    array.push(...this.additionalMethode);
    return array;
  }

  /**
   * Get mock technology list
   */
  getAllTechnologies(): string[] {
    var array = ['Jenkins', 'JAVA 7', 'JAVA 8', 'HTML 5', 'CSS 3', 'PHP 7', 'Talend'];
    array.push(...this.additionalTechnologie);
    return array;
  }

  /**
   * Get mock sheet file by it's id
   * @param id The sheet id
   */
  getFile(id: number) {
    throw new Error("Method not implemented.");
  }

  /**
   * Add a sheet to the mock sheet list
   * @param sheet The sheet to add
   */
  addSheet(sheet: Sheet) {
    if(sheet != undefined) {
      //Find the sheet by the id
      var sheetFind = this.sheets.find(obj => {
        return obj.id === +sheet.id
      });
      //If the sheet is not found, we add it
      if(sheetFind == undefined) {
        this.editMode = false;
        this.sheets.push(sheet);

        this.initCurrentSheet();
      }
    }
  }

  /**
   * Reset and initiatilize the current mock sheet
   */
  private initCurrentSheet() {
    this.sheet = new Sheet(-1);
    this.sheetSource.next(this.sheet);
  }
}
