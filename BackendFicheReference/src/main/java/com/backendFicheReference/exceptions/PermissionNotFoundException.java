package com.backendFicheReference.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PermissionNotFoundException extends RuntimeException {

    /**
     * Identifiant de serialisation
     */


    private static final String PERMISSION_NON_TROUVEE = "Permission non trouvée";
    private static final String PERMISSION_SUPPRIMEE = "Votre permission a été supprimée";

    public PermissionNotFoundException(String message) {
        super(message);
    }

    public static PermissionNotFoundException permissionNotFound() {
        return new PermissionNotFoundException(PERMISSION_NON_TROUVEE);
    }

    public static PermissionNotFoundException permissionSupprimee() {
        return new PermissionNotFoundException(PERMISSION_SUPPRIMEE);
    }
}

