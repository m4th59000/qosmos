import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
  providedIn: 'root'
})
/**
 * Guard for not logged
 */
export class NotLoggedGuard implements CanDeactivate<CanComponentDeactivate> {
  /**
   * Constructor
   * @param auth 
   */
  constructor(private auth: AuthService) {}

  /**
   * Deactivate the route if the user is authenticated
   * @param component 
   * @param currentRoute 
   * @param currentState 
   * @param nextState 
   */
  canDeactivate(component: CanComponentDeactivate, 
                currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot,
                nextState?: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    return this.auth.isAuth();
  }
}
