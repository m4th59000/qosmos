import {Component, OnInit} from '@angular/core';
import {SheetService} from '../../../../services/sheet.service';
import {Sheet} from '../../../../models/sheet.model';
import { ActivatedRoute, Router } from '@angular/router';
import { MockSheetService } from 'src/app/NeoSoftBordeau2018/services/mock-sheet.service';

@Component({
  selector: 'app-sheet-steps',
  templateUrl: 'steps.component.html',
  styleUrls: ['steps.component.scss']
})
/**
 * Component class for sheet form pages managing
 */
export class StepsComponent implements OnInit {
  //Determine if the global sheet form is hidden
  public stepGlobalHidden = false;
  //Determine if the mission sheet form is hidden
  public stepMissionHidden = true;
  //Determine if the additional sheet form is hidden
  public stepAdditionalHidden = true;

  public selectedMod = 'mission'; // mod mission by default

  //The current sheet to complete
  sheet: Sheet;

  private sub: any;
  private receivedSheetId = -1;

  /**
   * Constructor
   * @param sheetService
   * @param route
   * @param router
   */
  constructor(private sheetService: MockSheetService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  /**
   * Initialisation of the component
   */
  ngOnInit() {
    //Get the sheet id from parameters if it's present, for sheet edition
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.receivedSheetId = +params['id'] || -1;
    });

    //Subscribe to the current sheet from the sheet service
    this.sheetService.currentSheet.subscribe(sheet => this.sheet = sheet);

    //Initialize the edition mode
    if(this.receivedSheetId > -1) {
      if(!this.loadSheet(this.receivedSheetId)) {
        alert("La fiche " + this.receivedSheetId + " n'a pas pu être chargée.");
        this.router.navigate(['sheets/view'], { queryParams: { id: this.receivedSheetId } });
      } else {
        this.sheetService.editMode = true;
      }
    }
  }

  /**
   * Load the sheet to edit values in the form
   * @param id The sheet id to load
   */
  private loadSheet(id: number) {
    //Get the sheet by it's id from the sheet service
    let tempSheet = this.sheetService.getSingleSheet(this.receivedSheetId);

    if(tempSheet == undefined) {
      return false;
    } else {
      this.sheet.id = tempSheet.id;
      this.sheet.name = tempSheet.name;
      this.sheet.agence = tempSheet.agence;
      this.sheet.client = tempSheet.client;
      this.sheet.poste = tempSheet.poste;
      this.sheet.nombreCollaborateurs = tempSheet.nombreCollaborateurs;
      this.sheet.activites = tempSheet.activites;
      this.sheet.methodes = tempSheet.methodes;
      this.sheet.technologies = tempSheet.technologies;
      this.sheet.avantages = tempSheet.avantages;
      this.sheet.contextesEtEnjeux = tempSheet.contextesEtEnjeux;
      this.sheet.realisations = tempSheet.realisations;

      return true;
    }
  }

  /**
   * Select the mode which reprensent the step in the sheet form
   * @param mod The mode to select
   */
  selectMod(mod: string) {
    this.selectedMod = mod;
  }

  /**
   * Receive the message from the component child, in order to change the step in the form
   * @param $event
   */
  receiveMessage($event) {
    if ($event === 'goStepMission') {
      this.goStepMission();
    } else if ($event === 'goStepGlobal') {
      this.goStepGlobal();
    } else if ($event === 'goStepAdditional') {
      this.goStepAdditional();
    }
  }

  /**
   * Display the step mission page
   */
  goStepMission() {
    this.stepGlobalHidden = true;
    this.stepMissionHidden = false;
    this.stepAdditionalHidden = true;
  }

  /**
   * Display the step global page
   */
  goStepGlobal() {
    this.stepGlobalHidden = false;
    this.stepMissionHidden = true;
    this.stepAdditionalHidden = true;
  }

  /**
   * Display the step additional page
   */
  goStepAdditional() {
    this.stepGlobalHidden = true;
    this.stepMissionHidden = true;
    this.stepAdditionalHidden = false;
  }

  /**
   * Get the percentage of not empty fields in the form
   */
  getPercentage() {
    // for fun, works with this specific amount of properties ( 10 )
    // TODO optimize
    return this.getCountOfNotEmptyProperties() * 10 + '%';
  }

  /**
   * Get the count of not empty fields in the form
   */
  getCountOfNotEmptyProperties() {
    // TODO optimize
    let count = 0;
    if (this.sheet.agence !== undefined && this.sheet.agence !== '') {
      count++;
    }
    if (this.sheet.client !== undefined && this.sheet.client !== '') {
      count++;
    }
    if (this.sheet.poste !== undefined && this.sheet.poste !== '') {
      count++;
    }

    if (this.sheet.activites.length > 0) {
      count++;
    }
    if (this.sheet.methodes.length > 0) {
      count++;
    }
    if (this.sheet.nombreCollaborateurs !== undefined && this.sheet.nombreCollaborateurs !== '') {
      count++;
    }
    if (this.sheet.technologies.length > 0) {
      count++;
    }

    if (this.sheet.contextesEtEnjeux !== undefined && this.sheet.contextesEtEnjeux !== '') {
      count++;
    }
    if (this.sheet.realisations !== undefined && this.sheet.realisations !== '') {
      count++;
    }
    if (this.sheet.avantages !== undefined && this.sheet.avantages !== '') {
      count++;
    }

    return count;
  }

  getEditMode(): boolean {
    return this.sheetService.editMode;
  }
}
