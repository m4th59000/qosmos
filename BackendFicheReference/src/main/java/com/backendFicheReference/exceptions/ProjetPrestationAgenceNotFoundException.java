package com.backendFicheReference.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ProjetPrestationAgenceNotFoundException extends RuntimeException {

    /**
     * Identifiant de serialisation
     */


    private static final String PROJETPRESTATIONAGENCE_NON_TROUVEE = "ProjetPrestationAgence non trouvée";
    private static final String PROJETPRESTATIONAGENCE_SUPPRIMEE = "Votre projetPrestationAgence a été supprimée";

    public ProjetPrestationAgenceNotFoundException(String message) {
        super(message);
    }

    public static ProjetPrestationAgenceNotFoundException projetPrestationAgenceNotFound() {
        return new ProjetPrestationAgenceNotFoundException(PROJETPRESTATIONAGENCE_NON_TROUVEE);
    }

    public static ProjetPrestationAgenceNotFoundException projetPrestationAgenceSupprimee() {
        return new ProjetPrestationAgenceNotFoundException(PROJETPRESTATIONAGENCE_SUPPRIMEE);
    }
}

