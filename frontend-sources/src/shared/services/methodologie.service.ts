import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Methodologie} from "../models/Methodologie.model";
import {environment} from "../../environments/environment";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";

const baseUrl = environment.endpoint;
const path = 'methodologies/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class MethodologieService {

  constructor(private _http: HttpClient) { }

  getMethodologie(id: string) {
    return this._http.get<Methodologie>(baseUrl + path + id , {'headers': headers});
  }
  getMethodologies(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  createMethodologie(methodologie: Methodologie) {
    return this._http.post<Methodologie>(baseUrl + path , methodologie , {'headers': headers});
  }
  updateMethodologie(methodologie: Methodologie) {
    return this._http.put<Methodologie>(baseUrl + path , methodologie , {'headers': headers});
  }
}

