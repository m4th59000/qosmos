import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {ProjetPrestationAgence} from "../models/ProjetPrestationAgence.model";
import {environment} from "../../environments/environment";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";
import {Client} from "../models/Client.model";
import {AffichageFicheReferenceComponent} from "../../app/components/affichageFicheReference/affichageFicheReference.component";

const baseUrl = environment.endpoint;
const path = 'projetPrestationAgences/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class ProjetPrestationAgenceService {

  constructor(private _http: HttpClient) { }

  getProjetPrestationAgence(idProjet: string, idPrestation: string, idAgence: string) {
    return this._http.get<ProjetPrestationAgence>(baseUrl + path + idProjet + '/' + idPrestation + '/' + idAgence  , {'headers': headers});
  }

  getByProjet(idProjet: number){
    return this._http.get<ProjetPrestationAgence[]>(baseUrl + path + '/' + idProjet , {'headers': headers});
  }

  getProjetPrestationAgences(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  createProjetPrestationAgence(projetPrestationAgence: ProjetPrestationAgence) {
    return this._http.post<ProjetPrestationAgence>(baseUrl + path , projetPrestationAgence , {'headers': headers});
  }

  updateProjetPrestationAgence(projetPrestationAgence: ProjetPrestationAgence) {
    return this._http.put<ProjetPrestationAgence>(baseUrl + path , projetPrestationAgence , {'headers': headers});
  }

  getAffichageProjetPrestationAgenceComponent(idProjet: string, idPrestation: string, idAgence: string){
    return this._http.get<AffichageFicheReferenceComponent>(baseUrl + path + idProjet + '/' + idPrestation + '/' + idAgence, {"headers": headers});
  }
}



