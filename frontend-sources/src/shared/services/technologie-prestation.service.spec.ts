import {inject, TestBed} from '@angular/core/testing';

import { TechnologiePrestationService } from './technologie-prestation.service';

describe('TechnologiePrestationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [TechnologiePrestationService]
  }));

  it('should be created',  inject( [TechnologiePrestationService] , () => {
    const service: TechnologiePrestationService = TestBed.get(TechnologiePrestationService);
    expect(service).toBeTruthy();
  }));
});


