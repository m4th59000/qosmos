import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Technologie} from "../models/Technologie.model";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";

const baseUrl = environment.endpoint;
const path = 'technologies/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class TechnologieService {

  constructor(private _http: HttpClient) { }

  getTechnologie(id: string) {
    return this._http.get<Technologie>(baseUrl + path + id , {'headers': headers});
  }
  getTechnologieServices(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  createTechnologieService(technologieService: Technologie) {
    return this._http.post<Technologie>(baseUrl + path , technologieService , {'headers': headers});
  }
  updateTechnologieService(technologieService: Technologie) {
    return this._http.put<Technologie>(baseUrl + path , technologieService , {'headers': headers});
  }
}

