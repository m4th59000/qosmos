import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepMissionComponent } from './step-mission.component';

describe('StepMissionComponent', () => {
  let component: StepMissionComponent;
  let fixture: ComponentFixture<StepMissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepMissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepMissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
