import {AfterViewInit, Component, Input, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {ProjetPrestationAgence} from "../../../shared/models/ProjetPrestationAgence.model";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {ProjetPrestationAgenceService} from "../../../shared/services/projet-prestation-agence.service";
import {merge, of as observableOf} from 'rxjs';
import {Router} from "@angular/router";
import {PrestationMethodologieService} from "../../../shared/services/prestation-methodologie.service";
import {TechnologiePrestationService} from "../../../shared/services/technologie-prestation.service";



@Component({
  selector: 'app-fiche-reference-list',
  templateUrl: './fiche-reference-list.component.html',
  styleUrls: ['./fiche-reference-list.component.css']
})
export class FicheReferenceListComponent implements AfterViewInit {
  displayedColumns = ["projet", "client", "agence", "menDays", "periodeIntervention", "action"];
  dataSource: MatTableDataSource<ProjetPrestationAgence> = new MatTableDataSource<ProjetPrestationAgence>();
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  sortedBy = '';
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private _projetPrestationAgenceService: ProjetPrestationAgenceService,
              private prestationMethodologieService: PrestationMethodologieService,
              private technologiePrestationService: TechnologiePrestationService,
              private router: Router
  ) {
  }

  ngAfterViewInit() {
    this.bindData();
  }

  bindData() {
    console.log('binded');

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.loadDataSource();
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.totalElements;

          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
      this.dataSource.data = data
      console.log(this.dataSource.data);
    });

  }

  loadDataSource() {

    return this._projetPrestationAgenceService.getProjetPrestationAgences(this.paginator.pageIndex, this.paginator.pageSize,
      this.sort.active == 'projet' ? 'projet.nomProjet' :
        this.sort.active == 'client' ? 'projet.client.societe' :
          this.sort.active == 'agence' ? 'agence.agence' :
            this.sort.active == 'menDays' ? 'menDays' :
              this.sort.active == 'periodeIntervention' ? 'periodeIntervention' : ''
      , this.sort.direction);
  }

  /**
   * Redirect to the visuel fiche reference page
   */
  onGoVisuel(projetPrestationAgence: ProjetPrestationAgence) {
    this.router.navigate(['/sheets/view'], {
      queryParams:
        {
          idAgence: projetPrestationAgence.agence.id,
          idPrestation: projetPrestationAgence.prestation.id,
          idProjet: projetPrestationAgence.projet.id,
        }

    });
  }
}


