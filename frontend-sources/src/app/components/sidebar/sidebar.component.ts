import {Component} from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: 'sidebar.component.html',
  styleUrls: ['sidebar.component.scss'],
})
/**
 * Component class for sidebar page managing
 */
export class SidebarComponent {
}
