import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import {User} from "../../../shared/models/User.model";
import {AuthenticationService, UserService} from "../../../shared/_services";
import {FicheReferenceAPI} from "../../../shared/models/FicheReferenceAPI.model";



@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit {
  currentUser: User;
  users: FicheReferenceAPI = [];

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
    this.loadAllUsers();
  }

  deleteUser(id: number) {
    this.userService.delete(id)
      .pipe(first())
      .subscribe(() => this.loadAllUsers());
  }

  private loadAllUsers() {
    this.userService.getAll(0,100,'id','asc')
      .pipe(first())
      .subscribe(users => this.users = users);
  }
}
