package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.Permission;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface PermissionService {
    Optional<Permission> findPermission(Long id);
    Permission savePermission(Permission permission);
    Permission updatePermission(Permission permission , Long id);
    Page<Permission> permissions(String page , String size, String sortedBy, String sortedIn);
}