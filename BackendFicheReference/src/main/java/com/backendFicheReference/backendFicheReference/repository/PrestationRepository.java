package com.backendFicheReference.backendFicheReference.repository;


import com.backendFicheReference.backendFicheReference.model.Prestation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PrestationRepository extends JpaRepository<Prestation, Long> {
    Optional<Prestation> findById(Long id);
    Page<Prestation> findAll(Pageable pageable);
}