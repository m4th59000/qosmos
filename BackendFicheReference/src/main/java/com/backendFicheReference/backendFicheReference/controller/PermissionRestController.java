package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.Permission;
import com.backendFicheReference.backendFicheReference.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PermissionRestController{
    @Autowired
    private PermissionService permissionService;
    @RequestMapping(value = "permissions/{id}", method = RequestMethod.GET)
    public Optional<Permission> findPermission(@PathVariable("id") Long id){
        return permissionService.findPermission(id);
    }

    @RequestMapping(value = "/permissions", method = RequestMethod.GET)
    public Page<Permission> listPermissions(@RequestParam(name = "page" , defaultValue = "0") String page
            , @RequestParam(name = "size" , defaultValue = "10")  String size
            , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
            , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){
        return permissionService.permissions(page , size,sortedBy , sortedIn);
    }

    @RequestMapping(value = "/permissions", method = RequestMethod.POST)
    public Permission findPermission(@RequestBody Permission permission){
        return permissionService.savePermission(permission);
    }

    @RequestMapping(value = "/permissions/{id}", method = RequestMethod.PUT)
    public Permission updatePermission(@RequestBody Permission permission , @PathVariable("id") @NotNull Long id ){
        return permissionService.updatePermission(permission,id);
    }
}
