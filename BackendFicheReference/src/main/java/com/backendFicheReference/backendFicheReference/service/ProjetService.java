package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.Projet;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface ProjetService {
    Optional<Projet> findProjet(Long id);
    Projet saveProjet(Projet projet);
    Projet updateProjet(Projet projet , Long id);
    Page<Projet> projets(String page , String size, String sortedBy, String sortedIn);
}
