import {Agence} from './Agence.model';
import {Prestation} from './Prestation.model';
import {Projet} from './Projet.model';



export class ProjetPrestationAgence {
  constructor(
    public projet?: Projet,
    public prestation?: Prestation,
    public agence?: Agence,
    public menDays?: number,
    public periodeIntervention?: Date
  ) {}
}
