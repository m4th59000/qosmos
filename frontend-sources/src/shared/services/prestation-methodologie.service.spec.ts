import {inject, TestBed} from '@angular/core/testing';

import { PrestationMethodologieService } from './prestation-methodologie.service';

describe('PrestationMethodologieService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [PrestationMethodologieService]
  }));

  it('should be created', inject( [PrestationMethodologieService] , () => {
    const service: PrestationMethodologieService = TestBed.get(PrestationMethodologieService);
    expect(service).toBeTruthy();
  }));
});

