package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.Projet;
import com.backendFicheReference.backendFicheReference.service.ProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProjetRestController{
    @Autowired
    private ProjetService projetService;
    @RequestMapping(value = "projets/{id}", method = RequestMethod.GET)
    public Optional<Projet> findProjet(@PathVariable("id") Long id){
        return projetService.findProjet(id);
    }

    @RequestMapping(value = "/projets", method = RequestMethod.GET)
    public Page<Projet> listProjets(@RequestParam(name = "page" , defaultValue = "0") String page
            , @RequestParam(name = "size" , defaultValue = "10")  String size
            , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
            , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){
        return projetService.projets(page , size,sortedBy , sortedIn);
    }

    @RequestMapping(value = "/projets", method = RequestMethod.POST)
    public Projet findProjet(@RequestBody Projet projet){
        return projetService.saveProjet(projet);
    }

    @RequestMapping(value = "/projets/{id}", method = RequestMethod.PUT)
    public Projet updateProjet(@RequestBody Projet projet , @PathVariable("id") @NotNull Long id ){
        return projetService.updateProjet(projet,id);
    }

}
