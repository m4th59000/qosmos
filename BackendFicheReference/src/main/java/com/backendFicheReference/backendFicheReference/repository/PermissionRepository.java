package com.backendFicheReference.backendFicheReference.repository;


import com.backendFicheReference.backendFicheReference.model.Permission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    Optional<Permission> findById(Long id);
    Page<Permission> findAll(Pageable pageable);
}