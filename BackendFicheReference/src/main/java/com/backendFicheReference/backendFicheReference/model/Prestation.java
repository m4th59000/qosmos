package com.backendFicheReference.backendFicheReference.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "PRESTATION")


public class Prestation implements Serializable {

    // champ metier

    /**
    * Identifiant de serialisation
    */
    private static final long serialVersionUID = 1717266092505546704L;

    @Id
    @GeneratedValue(generator = "SEQ_PRESTATION_ID", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String activiteRealise;
    private String activiteSpecifique;
    private String activiteMetier;
    private Long caPrestation;

    private String reussite;
    private TypeEngagement typeEngagement;

}