package com.backendFicheReference.backendFicheReference.service.impl;

import com.backendFicheReference.backendFicheReference.model.Technologie;
import com.backendFicheReference.backendFicheReference.repository.TechnologieRepository;
import com.backendFicheReference.backendFicheReference.service.TechnologieService;
import com.backendFicheReference.exceptions.TechnologieNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TechnologieServiceImpl implements TechnologieService {

    @Autowired
    private TechnologieRepository technologieRepository;

    @Override
    public Optional<Technologie> findTechnologie(Long id) {
        final Optional<Technologie> optionalTechnologie = this.technologieRepository.findById(id);

        if (optionalTechnologie.isPresent()) {
            final Technologie technologie = optionalTechnologie.get();
            // convertir en TechnologieDTO
            // final TechnologieDTO dto = new TechnologieDTO(technologie);
            // final TechnologieDTO dto = TechnologieUtils.convert(technologie, "findById");
            return technologieRepository.findById(id);
        } else {
            throw TechnologieNotFoundException.technologieNotFound();
        }
    }

    @Override
    public Technologie saveTechnologie(Technologie technologie) {
        return technologieRepository.save(technologie);
    }

    @Override
    public Technologie updateTechnologie(Technologie technologie, Long id) {
        technologie.setId(id);
        return  technologieRepository.save(technologie);
    }

    @Override
    public Page<Technologie> technologies(String page , String size, String sortedBy, String sortedIn){
        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return technologieRepository.findAll(pageable);
    }

}