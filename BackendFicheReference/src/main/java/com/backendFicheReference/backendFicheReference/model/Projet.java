package com.backendFicheReference.backendFicheReference.model;

import java.io.Serializable;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "PROJET")


public class Projet implements Serializable {

    // champ metier

    /**
     * Identifiant de serialisation
     */
    private static final long serialVersionUID = 1810804864531181123L;

    @Id
    @GeneratedValue(generator = "SEQ_PROJET_ID", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String nomProjet;
    private String description;
    private String facteurCleSucces;
    private String codeCEGID;
    private Double etp;
    private String marquage;
    @ManyToOne
    private Client client;
}