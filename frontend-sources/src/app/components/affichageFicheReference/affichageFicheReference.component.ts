import {Component,OnInit} from '@angular/core';
import {Technologie} from "../../../shared/models/Technologie.model";
import {Methodologie} from "../../../shared/models/Methodologie.model";
import {ProjetPrestationAgence} from "../../../shared/models/ProjetPrestationAgence.model";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjetPrestationAgenceService} from "../../../shared/services/projet-prestation-agence.service";
import {PrestationMethodologie} from "../../../shared/models/PrestationMethodologie.model";
import {PrestationMethodologieService} from "../../../shared/services/prestation-methodologie.service";
import {TechnologiePrestationService} from "../../../shared/services/technologie-prestation.service";
import {TechnologiePrestation} from "../../../shared/models/TechnologiePrestation.model";
import html2canvas from "html2canvas";
import PptxGenJS from "pptxgenjs";
import {data} from "jquery";


@Component({
  selector: 'app-affichage-fiche-reference',
  templateUrl: './affichageFicheReference.component.html',
  styleUrls: ['./affichageFicheReference.component.css']
})


export class AffichageFicheReferenceComponent implements OnInit {


  ficheReference: ProjetPrestationAgence;
  technologies: Technologie[];
  methodologies: Methodologie[];



  constructor(private router: Router, private route: ActivatedRoute,
              private projetPrestationAgenceService: ProjetPrestationAgenceService,
              private prestationMethodologieService: PrestationMethodologieService,
              private technologiePrestationService: TechnologiePrestationService
  ) {
  }

  ngOnInit(): void {
    const idAgence = this.route.snapshot.queryParams.idAgence;
    const idPrestation = this.route.snapshot.queryParams.idPrestation;
    const idProjet = this.route.snapshot.queryParams.idProjet;

    this.methodologies = [];
    this.technologies = [];


    this.projetPrestationAgenceService.getProjetPrestationAgence(idProjet, idPrestation, idAgence).subscribe(
      data => {
        this.ficheReference = data;
      }
    );

    this.prestationMethodologieService.getByPrestation(idPrestation).subscribe(
      (data: PrestationMethodologie[]) =>{
        data.forEach((prestationMethodologie: PrestationMethodologie) => {
          this.methodologies.push(prestationMethodologie.methodologie);
        });
      }
    );

    this.technologiePrestationService.getByPrestation(idPrestation).subscribe(
      (data: TechnologiePrestation[]) =>{
        data.forEach((technologiePrestation: TechnologiePrestation) => {
          this.technologies.push(technologiePrestation.technologie);
        });
      }
    );


  }

  onGoBack() {
    this.router.navigate(['/referentiel']);
  }

  capturescreen(){
    html2canvas(document.body).then((canvas) => {
      const getImage = canvas.toDataURL(); // default is png
      console.log(getImage);

        // 1. Create a new Presentation
        let pptx = new PptxGenJS();

        // 2. Add a Slide
        let slide = pptx.addSlide();

        // 3. Add one or more objects (Tables, Shapes, Images, Text and Media) to the Slide

        // EX: Image by local URL
        //slide.addImage({ path:'assets/images/Capture.PNG', x:1, y:1, w:8.0, h:4.0 });

        // EX: Image from remote URL
        //slide.addMedia({ path:'https://upload.wikimedia.org/wikipedia/en/a/a9/Example.jpg', x:1, y:1, w:6, h:4 })

        // EX: Image by data (pre-encoded base64)
        slide.addImage({data: getImage, x: 0, y: 0, w: 10.0, h: 5.5});

        // 4. Save the Presentation
        pptx.writeFile(data(canvas));
    });
  }
}
