import {Agence} from "./Agence.model";

export class User {
  constructor(
    public id?: number,
    public userName?: string,
    public lastName?: string,
    public firstName?: string,
    public trigrammeUser?: string,
    public password?: string,
    public email?: string,
    public token?: string,
    public role?: string
  ) {}
}
