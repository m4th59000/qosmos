import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {PrestationMethodologie} from "../models/PrestationMethodologie.model";
import {environment} from "../../environments/environment";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";

const baseUrl = environment.endpoint;
const path = 'prestationMethodologies/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class PrestationMethodologieService {

  constructor(private _http: HttpClient) { }

  getPrestationMethodologie(idPrestation: string, idMethodologie: string) {
    return this._http.get<PrestationMethodologie>(baseUrl + path + idPrestation + '/' + idMethodologie  , {'headers': headers});
  }
  getPrestationMethodologies(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  getByPrestation(idprestation: number){
    return this._http.get<PrestationMethodologie[]>(baseUrl + path + '/' +idprestation  , {'headers': headers});
  }

  getByMethodologie(idmethodologie: number){
    return this._http.get<PrestationMethodologie[]>( baseUrl + path + '/' +idmethodologie , {'headers': headers});
  }

  createPrestationMethodologie(prestationMethodologie: PrestationMethodologie) {
    return this._http.post<PrestationMethodologie>(baseUrl + path , prestationMethodologie , {'headers': headers});
  }
  updatePrestationMethodologie(prestationMethodologie: PrestationMethodologie) {
    return this._http.put<PrestationMethodologie>(baseUrl + path , prestationMethodologie , {'headers': headers});
  }
}


