package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.Methodologie;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface MethodologieService {
    Optional<Methodologie> findMethodologie(Long id);
    Methodologie saveMethodologie(Methodologie methodologie);
    Methodologie updateMethodologie(Methodologie methodologie , Long id);
    Page<Methodologie> methodologies(String page , String size, String sortedBy, String sortedIn);
}
