################################################################################
# Qosmos - Docker - Varnish Cache Container Configuration                      #
# Maintainer: Neo-Soft                                                         #
# @author: William Pinaud <william.pinaud@neo-soft.fr>                         #
################################################################################


vcl 4.0;

backend default {
    .host = "qosmos-qa-nginx";
    .port = "10111";
}

# A simple configuration that let everything pass
sub vcl_recv {
    return (pass);
}