import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SheetCartComponent } from './sheet-cart.component';

describe('SheetCartComponent', () => {
  let component: SheetCartComponent;
  let fixture: ComponentFixture<SheetCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SheetCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SheetCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
