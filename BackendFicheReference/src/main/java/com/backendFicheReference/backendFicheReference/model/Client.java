package com.backendFicheReference.backendFicheReference.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "CLIENT")


public class Client implements Serializable {

    // champ metier

    /**
     * Identifiant de serialisation
     */
    private static final long serialVersionUID = -535647917165205669L;

    @Id
    @GeneratedValue(generator = "SEQ_CLIENT_ID", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String societe;
    private String secteurActivite;
    private String imageClient;

}