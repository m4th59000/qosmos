package com.backendFicheReference.backendFicheReference.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "UTILISATEUR")


public class User implements Serializable {

    // champ metier

    /**
     * Identifiant de serialisation
     */
    private static final long serialVersionUID = -7333445456322271286L;

    @Id
    @GeneratedValue(generator = "SEQ_USER_ID", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String userName;
    private String firstName;
    private String lastName;
    private String trigrammeUser;
    private String password;
    private String email;
    private String token;
    private String role;
}