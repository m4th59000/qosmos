export enum TypeEngagement {
  'atg' = 'ATG',
  'at' = 'AT',
  'ate'='ATE',
  'cdr_cdc' = 'CDR_CDC',
  'cds' = 'CDS',
  'tma'='TMA',
  'forfait' = 'FORFAIT',
  'AuditConseil' = 'AuditConseil',

}
