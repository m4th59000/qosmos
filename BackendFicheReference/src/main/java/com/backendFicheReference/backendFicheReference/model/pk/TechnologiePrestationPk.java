package com.backendFicheReference.backendFicheReference.model.pk;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TechnologiePrestationPk implements Serializable {

    private Long idTechnologie;
    private Long idPrestation;

    public TechnologiePrestationPk() {
    }

    public TechnologiePrestationPk(Long idTechnologie, Long idPrestation) {
        this.idTechnologie = idTechnologie;
        this.idPrestation = idPrestation;
    }

    public Long getIdTechnologie() {
        return idTechnologie;
    }

    public void setIdTechnologie(Long idTechnologie) {
        this.idTechnologie = idTechnologie;
    }

    public Long getIdPrestation() {
        return idPrestation;
    }

    public void setIdPrestation(Long idPrestation) {
        this.idPrestation = idPrestation;
    }
}
