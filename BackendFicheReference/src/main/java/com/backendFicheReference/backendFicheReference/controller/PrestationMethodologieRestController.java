package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.PrestationMethodologie;
import com.backendFicheReference.backendFicheReference.model.pk.PrestationMethodologiePk;
import com.backendFicheReference.backendFicheReference.service.PrestationMethodologieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PrestationMethodologieRestController {

    @Autowired
    private PrestationMethodologieService prestationMethodologieService;
    @RequestMapping(value = "prestationMethodologies/{idMethodologie}/{idPrestation}", method = RequestMethod.GET)
    public Optional<PrestationMethodologie> findPrestationMethodologie(@PathVariable("idMethodologie") @NotNull Long idMethodologie,
                                                                       @PathVariable("idPrestation") @NotNull Long idPrestation
    ){
        return prestationMethodologieService.findPrestationMethodologie(new PrestationMethodologiePk(idMethodologie,idPrestation) );
    }

    @RequestMapping(value = "prestationMethodologies/{idprestation}", method = RequestMethod.GET)
    public List<PrestationMethodologie> findPrestationMethodologie(@PathVariable("idprestation") Long idprestation
                                                                       ){
        return prestationMethodologieService.findByPrestation(idprestation);
    }

    @RequestMapping(value = "/prestationMethodologies", method = RequestMethod.GET)
    public Page<PrestationMethodologie> listPrestationMethodologies(@RequestParam(name = "page" , defaultValue = "0") String page
            , @RequestParam(name = "size" , defaultValue = "10")  String size
            , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
            , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){
        return prestationMethodologieService.prestationMethodologies(page , size,sortedBy , sortedIn);
    }

    @RequestMapping(value = "/prestationMethodologies", method = RequestMethod.POST)
    public PrestationMethodologie findPrestationMethodologie(@RequestBody PrestationMethodologie prestationMethodologie){
        return prestationMethodologieService.savePrestationMethodologie(prestationMethodologie);
    }

    @RequestMapping(value = "/prestationMethodologies/{idMethodologie}/{idPrestation}", method = RequestMethod.PUT)
    public PrestationMethodologie updatePrestationMethodologie(@RequestBody PrestationMethodologie prestationMethodologie,
                                                             @PathVariable("idMethodologie") @NotNull Long idMethodologie,
                                                             @PathVariable("idPrestation") @NotNull Long idPrestation
    ){
        return prestationMethodologieService.updatePrestationMethodologie(prestationMethodologie, new PrestationMethodologiePk(idMethodologie,idPrestation));
    }
}





