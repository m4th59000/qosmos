import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepGlobalComponent } from './step-global.component';

describe('StepGlobalComponent', () => {
  let component: StepGlobalComponent;
  let fixture: ComponentFixture<StepGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
