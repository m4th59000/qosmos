import { Sheet } from '../models/sheet.model';
import { BehaviorSubject } from 'rxjs';

/**
 * Generic class for sheet service
 */
export abstract class GenericSheetService {

    //Additional fields added by the user in the forms
    additionalAgences = [];
    additionalClients = [];
    additionalPostes = [];
    additionalActivite = [];
    additionalMethode = [];
    additionalTechnologie = [];

    abstract getAllSheets();
    abstract getSingleSheet(id: number): Sheet;
    abstract getAllAgences(): string[];
    abstract getAllClients(): string[];
    abstract getAllPostes(): string[];
    abstract getAllActivites(): string[];
    abstract getAllMethods(): string[];
    abstract getAllTechnologies(): string[];
    abstract getFile(id: number);
    abstract addSheet(sheet: Sheet);
}
