package com.backendFicheReference.backendFicheReference.service.impl;


import com.backendFicheReference.backendFicheReference.model.User;
import com.backendFicheReference.backendFicheReference.repository.UserRepository;
import com.backendFicheReference.backendFicheReference.service.UserService;
import com.backendFicheReference.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<User> findUser(Long id) {
        final Optional<User> optionalUser = this.userRepository.findById(id);

        if (optionalUser.isPresent()) {
            final User user = optionalUser.get();
            // convertir en UserDTO
            // final UserDTO dto = new UserDTO(user);
            // final UserDTO dto = UserUtils.convert(user, "findById");
            return userRepository.findById(id);
        } else {
            throw UserNotFoundException.userNotFound();
        }
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User updateUser(User user, Long id) {
        user.setId(id);
        return userRepository.save(user);
    }

    @Override
    public Page<User> users(String page , String size, String sortedBy, String sortedIn){
        Pageable pageable ;
        if(sortedIn.equals("asc")) {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).ascending());
        }
        else {
            pageable = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(sortedBy).descending());
        }
        return userRepository.findAll(pageable);
    }

}