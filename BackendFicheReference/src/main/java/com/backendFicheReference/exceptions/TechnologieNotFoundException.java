package com.backendFicheReference.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TechnologieNotFoundException extends RuntimeException {

    /**
     * Identifiant de serialisation
     */


    private static final String TECHNOLOGIE_NON_TROUVEE = "Technologie non trouvée";
    private static final String TECHNOLOGIE_SUPPRIMEE = "Votre technologie a été supprimée";

    public TechnologieNotFoundException(String message) {
        super(message);
    }

    public static TechnologieNotFoundException technologieNotFound() {
        return new TechnologieNotFoundException(TECHNOLOGIE_NON_TROUVEE);
    }

    public static TechnologieNotFoundException technologieSupprimee() {
        return new TechnologieNotFoundException(TECHNOLOGIE_SUPPRIMEE);
    }
}

