package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.Agence;
import com.backendFicheReference.backendFicheReference.service.AgenceService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AgenceRestController{
    private final AgenceService agenceService;

    public AgenceRestController(AgenceService agenceService) {
        this.agenceService = agenceService;
    }

    @GetMapping( "agences/{id}" )
    public Optional<Agence> findAgence(@PathVariable("id") Long id
    ){
        return agenceService.findAgence(id);
    }

    @RequestMapping(value = "/agences", method = RequestMethod.GET )
    public Page<Agence> listAgences(@RequestParam(name = "page" , defaultValue = "0") String page
            , @RequestParam(name = "size" , defaultValue = "10")  String size
            , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
            , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){

        return agenceService.agences(page , size,sortedBy , sortedIn);
    }


    @RequestMapping(value = "/agences", method = RequestMethod.POST)
    public Agence findAgence(@RequestBody Agence agence){
        return agenceService.saveAgence(agence);
    }

    @RequestMapping(value = "/agences/{id}", method = RequestMethod.PUT)
    public Agence updateAgence(@RequestBody Agence agence , @PathVariable("id") @NotNull Long id ){
        return agenceService.updateAgence(agence,id);
    }
}
