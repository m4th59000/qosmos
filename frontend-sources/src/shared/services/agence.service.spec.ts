import {inject, TestBed} from '@angular/core/testing';

import { AgenceService } from './agence.service';

describe('AgenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [AgenceService]
  }));
});
