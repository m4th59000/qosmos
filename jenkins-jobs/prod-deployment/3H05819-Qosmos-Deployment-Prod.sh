cd
cp -Rfv jenkins/workspace/3H05819-Qosmos-Deployment-Prod/* qosmos-prod/
cd qosmos-prod
docker-compose -f docker/global-docker-compose.yml -f docker/prod-docker-compose.yml -p "qosmos-prod" up -d --build --force-recreate