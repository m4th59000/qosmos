import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Sheet} from '../../../../models/sheet.model';
import {SheetService} from '../../../../services/sheet.service';
import { MockSheetService } from 'src/app/NeoSoftBordeau2018/services/mock-sheet.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sheet-form-step-mission',
  templateUrl: 'step-mission.component.html',
  styleUrls: ['step-mission.component.scss']
})
/**
 * Component class for sheet form - step mission page managing
 */
export class StepMissionComponent implements OnInit {
  //Message event used to change the step of the form
  @Output() messageEvent = new EventEmitter<string>();

  //Agency list, used in the html select component 'selectAgence'
  activities = [''];
  //Agency list, used in the html select component 'selectAgence'
  methods = [''];
  //Agency list, used in the html select component 'selectAgence'
  technologies = [''];

  //The current sheet to complete
  sheet: Sheet;

  newActivite: string;
  newMethode: string;
  newTechnologie: string;

  /**
   * Constructor
   * @param sheetService
   * @param router
   */
  constructor(private sheetService: MockSheetService, private router: Router) { }

  /**
   * Initialisation of the component
   */
  ngOnInit() {
    //Subscribe to the current sheet service
    this.sheetService.currentSheet.subscribe(sheet => this.sheet = sheet);

    //Get the datas from the sheet service
    this.activities.push(...this.sheetService.getAllActivites());
    this.methods.push(...this.sheetService.getAllMethods());
    this.technologies.push(...this.sheetService.getAllTechnologies());
  }

  /**
   * Select a technology to add to the current sheet
   * @param technologie
   */
  onSelectTechnologie(technologie: any) {
    if (!this.sheet.technologies.includes(technologie) && technologie !== '') {
      this.sheet.technologies.push(technologie);
    }
  }

  /**
   * Select a method to add to the current sheet
   * @param method
   */
  onSelectMethod(method: any) {
    if (!this.sheet.methodes.includes(method) && method !== '') {
      this.sheet.methodes.push(method);
    }
  }

  /**
   * Select an actvity to add to the current sheet
   * @param activitie
   */
  onSelectActivitie(activitie: any) {
    if (!this.sheet.activites.includes(activitie) && activitie !== '') {
      this.sheet.activites.push(activitie);
    }
  }

  /**
   * Remove the given technology from the the selected values
   * @param elementToRemove
   */
  onClickSelectedBadgeTechnologie(elementToRemove: any) {
    const index: number = this.sheet.technologies.indexOf(elementToRemove);
    if (index !== -1) {
      this.sheet.technologies.splice(index, 1);
    }
  }

  /**
   * Remove the given method from the the selected values
   * @param elementToRemove
   */
  onClickSelectedBadgeMethod(elementToRemove: any) {
    const index: number = this.sheet.methodes.indexOf(elementToRemove);
    if (index !== -1) {
      this.sheet.methodes.splice(index, 1);
    }
  }

  /**
   * Remove the given activity from the the selected values
   * @param elementToRemove
   */
  onClickSelectedBadgeActivitie(elementToRemove: any) {
    const index: number = this.sheet.activites.indexOf(elementToRemove);
    if (index !== -1) {
      this.sheet.activites.splice(index, 1);
    }
  }

  /**
   * Get filtered activity list
   */
  getFilteredActivities() {
    return this.getFilteredList(this.activities, this.sheet.activites);
  }

  /**
   * Get filtered method list
   */
  getFilteredMethods() {
    return this.getFilteredList(this.methods, this.sheet.methodes);
  }

  /**
   * Get filtered technology list
   */
  getFilteredTechnologies() {
    return this.getFilteredList(this.technologies, this.sheet.technologies);
  }

  /**
   * Get filtered list
   * @param listToFilter
   * @param listToFilterWith
   */
  getFilteredList(listToFilter: any, listToFilterWith: any) {
    return listToFilter.filter(
      function (e) {
        return this.indexOf(e) < 0;
      },
      listToFilterWith
    );
  }

  /**
   * Redirect to the step global
   */
  goStepGlobal() {
    this.messageEvent.emit('goStepGlobal');
  }

  /**
   * Redirect to the step additional
   */
  goStepAdditional() {
    this.messageEvent.emit('goStepAdditional');
  }

  /**
   * Check if the form is invalid
   */
  formIsInvalid() {
    if ((this.sheet.activites.length > 0)
        && (this.sheet.methodes.length > 0)
        && (this.sheet.nombreCollaborateurs !== undefined && this.sheet.nombreCollaborateurs !== '')
        && (this.sheet.technologies.length > 0))
      return false;
    else
      return true;
  }

  /**
   * Add new value in the given list
   * @param newField new value to add
   * @param array the list which wait for the new value
   */
  addNewField(newField: string, array: string[]) {
    if(this.isValid(newField) && this.isNotExist(newField, array))
    {
      array.push(newField);

      if(array == this.activities) {
        this.sheetService.additionalActivite.push(newField);
        this.onSelectActivitie(newField);
      } else if(array == this.methods) {
        this.sheetService.additionalMethode.push(newField);
        this.onSelectMethod(newField);
      } else if(array == this.technologies) {
        this.sheetService.additionalTechnologie.push(newField);
        this.onSelectTechnologie(newField);
      }
    } else {
      console.log('already exist');
    }
  }

  /**
   * Check if the new value is valid
   * @param newField the value to check
   */
  private isValid(newField: string) {
    if(newField !== undefined && newField != '')
      return true;
    else
      return false;
  }

  /**
   * Check if the new value exist in the given list
   * @param newField the new value to check
   * @param array the list to check
   */
  private isNotExist(newField: string, array: string[]) {
    var exist = false;

    array.forEach((field) => {
      if(field.toLowerCase().includes('-')) {

        field.split('-').forEach((subfield) => {
          if(subfield.toLowerCase() == newField.toLowerCase())
            exist = true;
        });
      }
      if(field.toLowerCase() == newField.toLowerCase()) {
        exist = true;
      }
    });

    return !exist;
  }

  /**
   * Cancel the edition
   */
  onCancelEdition() {
    this.sheetService.editMode = false;
    this.router.navigate(['/sheet', 'form']);
  }

  getEditMode(): boolean {
    return this.sheetService.editMode;
  }
}
