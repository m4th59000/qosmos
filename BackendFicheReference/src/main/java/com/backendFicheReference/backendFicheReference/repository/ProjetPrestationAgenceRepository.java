package com.backendFicheReference.backendFicheReference.repository;

import com.backendFicheReference.backendFicheReference.model.ProjetPrestationAgence;
import com.backendFicheReference.backendFicheReference.model.pk.ProjetPrestationAgencePk;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProjetPrestationAgenceRepository extends JpaRepository<ProjetPrestationAgence, ProjetPrestationAgencePk> {
    Optional<ProjetPrestationAgence> findById(ProjetPrestationAgencePk id);
    Page<ProjetPrestationAgence> findAll(Pageable pageable);
}


