package com.backendFicheReference.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ProjetPrestationAgenceBadRequest extends RuntimeException {

    private static final String MAUVAIS_FORMAT_DATE = "Date dans un mauvais format";
    public ProjetPrestationAgenceBadRequest(String message) {
        super(message);
    }

    public static ProjetPrestationAgenceBadRequest projetPrestationAgenceBadRequest() {
        return new ProjetPrestationAgenceBadRequest(MAUVAIS_FORMAT_DATE);
    }
}


