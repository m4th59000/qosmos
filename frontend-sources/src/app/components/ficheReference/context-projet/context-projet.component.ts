import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Client} from "../../../../shared/models/Client.model";
import {ClientService} from "../../../../shared/services/client.service";
import {ProjetService} from "../../../../shared/services/projet.service";
import {Projet} from "../../../../shared/models/Projet.model";
import {Marquage} from "../../../../shared/models/Marquage.model";



@Component({
  selector: 'app-context-projet',
  templateUrl: './context-projet.component.html',
  styleUrls: ['./context-projet.component.css']
})
export class ContextProjetComponent implements OnInit {
  contextProjet: FormGroup;
  client: Client;
  selectedClientValue: Client;
  selectedMarquageValue: Marquage;
  clients: Client[];
  @Output() projetEvent = new EventEmitter<Projet>();
  @Output() contextProjetEvent = new EventEmitter<FormGroup>();
  constructor(private _formBuilder: FormBuilder , private _clientService: ClientService , private _projetService: ProjetService) { }

  ngOnInit(): void {
    this.buildContextProjet();
    this.loadClients();
  }

  buildContextProjet() {
    this.contextProjet = this._formBuilder.group({
      nomProjet : ['', Validators.required],
      description : ['', Validators.required],
      codeCEGID : ['', Validators.required],
      etp : ['', Validators.required],
      clientCreation : ['no'],
      client : ['', Validators.required],
      clientSociete : [null],
      clientSecteurActivite : [null],
      marquage:['', Validators.required],
    });

    this.setClientValidator();
    //after build send the form control to parent
    this.contextProjetEvent.emit(this.contextProjet);
  }

  enableClientCreation() {
    this.contextProjet.patchValue({
      clientCreation : ['yes'],
      client : [null]
    });
  }

  disableClientCreation() {
    this.contextProjet.patchValue({
      client : ['yes'],
      clientCreation : ['no'],
      clientSociete : [null],
      secteurActivite : [null],
    });
  }

  setClientValidator() {
    const client = this.contextProjet.get('client');
    const clientSociete = this.contextProjet.get('clientSociete');

    this.contextProjet.get('clientCreation').valueChanges.subscribe(
      clientCreation => {
        if (clientCreation === 'yes') {
          clientSociete.setValidators(null);
          client.setValidators([]);
        } else {
          client.setValidators(null);
          clientSociete.setValidators([]);
        }
      }
    );

    client.updateValueAndValidity();
    clientSociete.updateValueAndValidity();
  }

  loadClients() {
    this.client = new Client();
    this.clients = [];
    this._clientService.getClients(0,100,'id','asc').subscribe(
      clientList => {
        this.clients = clientList.content;
        console.log('clients loaded' + JSON.stringify( clientList.content));
      }
      , err => {
        console.log('error');
      }
    );
  }

  goCreateClient(){
    const client = new Client();
    client.societe = this.contextProjet.get('clientSociete').value;
    client.secteurActivite = this.contextProjet.get('clientSecteurActivite').value;

    this._clientService.createClient(client).subscribe(
      client => {
        this.contextProjet.patchValue({
          client : [client]
        });

        this.clients.push(client);
        this.selectedClientValue = client;
        console.log('client created');
    },
    err => {
        console.log('can t create client');
    }
    );
  }

  createProject() {
    const creationClient = this.contextProjet.get('clientCreation').value;
    console.log('Creation du client : ' + creationClient);

    if (creationClient == 'yes') {
      console.log('coucou');
      this.goCreateClient();
    }

    this._projetService.createProjet(this.contextProjet.getRawValue()).subscribe(
      projet => {
        this.projetEvent.emit(projet);
        console.log('project created');
      },
      err => {
        console.log('cant create project');
      }
    )
  }

}
