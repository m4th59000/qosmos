![](qosmos-project-logo.jpg)

# Qosmos

_________________

## 📣 1. Présentation du projet (les bases `^^`)

### 📑 Quelques généralités sur le projet

Le projet a pour but de fournir un site intranet permettant la saisie, 
la visualisation et le téléchargement de fiches de références.

Le projet est composé :
* D'un **frontend** développé en **Javascript / Angular 7**
* D'un **backend** développé en **PHP / Symfony 4**

* D'**un container** de **base de données relationnelle et document storage MySQL 8**
* D'**un container** **RabbitMQ** comme message **broker**
* D'**un container** **MailCatcher** pour les **emails** en développement
* D'**un container** **REDIS** pour le **cache sessions PHP et les templates Twig**
* D'**une grappe de containers** **NginX** comme **serveur web frontend et backend**
* D'**une grappe de containers** **PHP / PHP-FPM**
* D'**une grappe de containers** **Varnish Cache** comme **reverse proxy**
* D'**un container** **HAProxy** comme **load balancer**

_NB: le nom des images et containers Docker est normalement normalisé sous la forme :_
`[nom du projet]-[nom de l'environnement]-[nom du container]`, 
où chaque token entre crochets respecte la REGEX `/[a-zA-Z]+/`.

Les fichiers de déploiement du frontend sont présents dans le dossier `/frontend-built`.  
Les fichiers de déploiement du backend sont présents dans le dossier `/backend/public`.

### 🌌 Stack technologique pour travailler sur le projet

```
Environnement de travail de production
--------------------------------------

[Sur le poste de développement]
prod.qosmos.neo-soft.local          >> HAProxy > Varnish > NginX > Deployment Angular
cdn.prod.qosmos.neo-soft.local      >> HAProxy > Varnish > NginX > Deployment Angular
qosmos.neo-soft.local/backend/      >> HAProxy > Varnish > NginX > PHP-FPM

[Déployé]
qosmos.neo-soft.fr                  >> HAProxy (extérieur à ce projet) > HAProxy > Varnish > NginX > Deployment Angular
cdn.qosmos.neo-soft.fr              >> HAProxy (extérieur à ce projet) > HAProxy > Varnish > NginX > Deployment Angular
qosmos.neo-soft.fr/backend/         >> HAProxy (extérieur à ce projet) > HAProxy > Varnish > NginX > PHP-FPM


Environnement de travail de QA (recette)
----------------------------------------   

[Sur le poste de développement]
qa.qosmos.neo-soft.local            >> HAProxy > Varnish > NginX > Deployment Angular
cdn.qa.qosmos.neo-soft.local        >> HAProxy > Varnish > NginX > Deployment Angular
qa.qosmos.neo-soft.local/backend/   >> HAProxy > Varnish > NginX > PHP-FPM

[Déployé]
qa.qosmos.neo-soft.fr               >> HAProxy (extérieur à ce projet) > HAProxy > Varnish > NginX > Deployment Angular
cdn.qa.qosmos.neo-soft.fr           >> HAProxy (extérieur à ce projet) > HAProxy > Varnish > NginX > Deployment Angular
qa.qosmos.neo-soft.fr/backend/      >> HAProxy (extérieur à ce projet) > HAProxy > Varnish > NginX > PHP-FPM


Environnement de travail de Dev (développement)
----------------------------------------   
dev.qosmos.neo-soft.local           >> HAProxy > Varnish > NginX > Deployment Angular
cdn.dev.qosmos.neo-soft.local       >> HAProxy > Varnish > NginX > Deployment Angular
dev.qosmos.neo-soft.local/backend/  >> HAProxy > Varnish > NginX > PHP-FPM


Autres options locales (pour tester uniquement)
-----         
localhost:4200        > Deployment Angular (serveur NPM, "npm start" à la racine du répertoire /frontend-built
localhost:8000/api/   > Server intégré PHP (serveur PHP intégré, 
                        "php bin/console server:run [OPTIONS]" à la racine du répertoire /backend)

```

### 🌌 Configuration réseau du projet

#### Comprendre la logique de construction réseau du projet

Les containers Docker sont paramétrés avec des IP internes déterminées par Docker.  
A l'exception des frontaux HAProxy, qui sont exposés hors réseau privé Docker.

Les ports, en revanche, sont soumis à une norme purement arbitraire, 
déterminée par le projet et sa multitude de containers.

Pour cela, la mécanique retenue est la suivante :
* le réseau doit être **interne et privé**, 
sur un des réseaux des [RFC 1918](https://fr.wikipedia.org/wiki/R%C3%A9seau_priv%C3%A9)
* on utilisera le réseau **`10.0.0.0`**
* on utilisera le masque de sous-réseau **`/16`**, suffisant pour le nombre de containers envisagés
* on utilisera un **réseau différent** pour les environnements de *prod* et de *qa*
* on va **changer** et **spéficier tous les ports** des applications **exposés** dans le réseau Docker, 
ainsi qu'**à l'intérieur** des containers, ceci par souci de sécurité et de montée en compétence des équipes.
* les ports seront, pour rappeler les IPs, déterminés à partir de **`10000`**.
* les ports devront refléter : la **technologie** qu'ils représentent, 
la **chaîne load-balancée** sur laquelle ils sont, et **l'environnement** auquel ils sont rapportés.
* On a besoin de maximum **2 chaînes de load-balancing**, à des fins de montée en compétence, 
le projet n'a aucune montée en charge prévue.
* On aura **au maximum une poignée d'environnements (ici 3)**, 
sans quoi la méthodologie projet est ingérable pour les équipes.
* On aura **au maximum une dizaine de technologies** ayant besoin d'un port ouvert interne.
* On aura **au maximum une dizaine de projets** sur la même machine hôte 
(on peut donc s'affranchir des ports dynamiques).

#### Mnémotechnique pour retenir les ports (si besoin)

Ainsi, les ports sont sous la forme suivante :   
`1 [numéro de la technologie] [numéro de l'environnement] 
[numéro d'instance dans la chaîne à partir de 1, ou port additionnel]`.

Où, pour chaque token...

Pour `[numéro de la technologie]` (à compléter au fur et à mesure) :
* **`01`** représente **NginX** 
* **`02`** représente **PHP-FPM**
* **`03`** représente **MySQL**
* **`04`** représente **REDIS**
* **`05`** représente **ELK (le frontend HTTP de Kibana)**
* **`06`** représente **RabbitMQ**
* **`07`** représente **MailCatcher (SMTP input)**
* **`08`** représente **MailCatcher (frontend HTTP)**
* **`09`** représente **VarnishCache**
* **`10`** représente **ELK (le port d'input de LogStash)**
* **`11`** représente **ELK (le port HTTP d'ElasticSearch)**
* **`12`** représente **ELK (le port TCP de transport d'ElasticSearch)**

Pour `[numéro de l'environnement]` (à compléter au fur et à mesure) :
* **`0`** représente **la production**
* **`1`** représente **QA**
* **`2`** représente **dev** (pour bosser localement)

Pour `[numéro d'instance dans la chaîne à partir de 1, ou port additionnel]` (à compléter au fur et à mesure) :
* **`1`** représente **la première chaîne load-balancée de VarnishCache/NginX/PHP-FPM**
* **`2`** représente **la seconde chaîne load-balancée de VarnishCache/NginX/PHP-FPM**

*(exemple: `10321` réprésente l'instance de MySQL sur le rail load-balancé numéro 1 en développement/Dev)* 

Seules exceptions : comme on doit avoir plusieurs sites sur la même machine 
(QA/Prod sur la machine NS de QA/Prod et Dev/QA/Prod sur les machines de développement),
les ports pour le frontal final sont :

- **`5010`** pour le frontal **HAProxy de Prod**
- **`5011`** pour le frontal **HAProxy de QA**
- **`5012`** pour le frontal **HAProxy de Dev**

_________________


## 💢 2. Etapes liminaires

### 💻 Configuration système

Pour pouvoir travailler sur ce projet, il vous faudra les droits admin sur votre ordinateur. 
Faites-en faire la demande par votre reponsable projet sur le [support neosoft](https://mysupport.neo-soft.fr).

### 💻 IDE

Configurer l'IDE pour :
 - Répondre aux standards de codings...
    - De Symfony 4 pour le backend
    - Du W3C pour Javascript et ses dérivés (TypeScript / JSX / etc.)
 - Ajouter automatiquement les informations d'intput/output sur les classes, méthodes, fonctions
 - Lire et interpréter les fichiers Git et les types d'extensions utilisées sur le projet :
    - PHP
    - Javascript
    - TypeScript
    - YAML
    - .env
    - SCSS (avec si possible l'interpréteur / préprocesseur temps réel)

_________________


## 🔻 3. Installation
 
### ⚡ Setup local :

#### Step 1 - Configuration filesystem

D'abord, il vous faudra créer la structure suivante pour le projet :

```
 + [répertoire PROJET, genre "qosmos"]
 |
 |---+ docker-volumes-sources
 |   |
 |   +--- qosmos-qa
 |   |
 |   +--- qosmos-prod
 | 
 | 
 +---+ [répertoire GIT (issu d'un Git clone), genre "qosmos-git"]
```

Les répertoires `docker-volumes-sources/qosmos-prod` et `docker-volumes-sources/qosmos-qa` 
servent de stockage pour conserver les volumes (bases de données, fichiers uploadés, etc.) d'un build à l'autre.

#### Step 2 - Configuration Git

Le projet utilise des sources destinées à des machines Unix / Linux.
Les personnes travaillant sur le projet seront sous Windows 10.
Pour éviter les soucis de retours à la ligne différents et les versionnements dans Git, 
la première fois que le projet est installé, à la racine du projet, taper :

* `git config core.autocrlf false`
* `git config --global core.safecrlf false`

Ensuite, cloner le projet (il vous faut pour cela préalablement confirmer vos accès GitLab) :

* `git clone https://gitlab-projets.neo-soft.fr/3H-05819/qosmos.git [votre nom de répertoire GIT de l'étape 1]`

#### Step 3 - Setup pour travailler sur les sources frontend

Il vous faudra [installer node et npm](https://nodejs.org/en/) sur votre machine.

Pour exécuter l'application angular, allez dans le dossier `qosmos/frontend-sources`, faites les commandes :

* `npm install`
* `npm start`

#### Step 4 - Setup pour le backend

Il est possible, mais pas indispensable d'[installer Composer](https://getcomposer.org/download/) sur votre machine 
ainsi que [PHP](http://www.php.net/downloads.php) 
([comment installer](https://www.sitepoint.com/how-to-install-php-on-windows/)).

Pas indispensable, puisqu'il conviendra d'exécuter toutes les commandes depuis le container PHP-CLI, 
et non depuis sa machine hôte.

Pour cela, les commandes seront exécutées avec un user autre que root sur le container `[env]-php-cli`



#### Step 5 - Modifications du fichier hosts (ou plugin navigateur équivalent)

Modifier son fichier hosts (`/etc/hosts` sous Linux, 
`[lecteur amorce]:\Windows\System32\drivers\etc\hosts` sous Windows) 
et y ajouter :

```
# Qosmos -- local URLs -- routed to respective environments

# "prod" environment on local devices
127.0.0.1 prod.qosmos.neo-soft.ns
127.0.0.1 cdn.prod.qosmos.neo-soft.ns

# "qa" environment on local devices
127.0.0.1 qa.qosmos.neo-soft.ns
127.0.0.1 cdn.qa.qosmos.neo-soft.ns

# "dev" environment on local devices
127.0.0.1 dev.qosmos.neo-soft.ns
127.0.0.1 cdn.dev.qosmos.neo-soft.ns
```

#### Step 6 - Installation du site via Docker :

Il vous faudra [installer Docker For Windows](https://docs.docker.com/docker-for-windows/install/) 
sur votre machine (requiert les droits admin).

Ensuite, se positionner dans le répertoire ```/[votre répertoire GIT de l'étape 1]``` (racine) du répertoire Git 
et lancer la commande Shell Windows correspondant à ce que vous voulez faire 
depuis une console Windows (CMD / Powershell / Unix shell) :
- ```build-all-docker-containers.bat``` pour (re)construire les images de **tous** les environnements et les lancer.
- ```build-prod-docker-containers.bat``` pour (re)construire les images des environnements de **PROD** et les lancer.
- ```build-qa-docker-containers.bat``` pour (re)construire les images des environnements de **QA** et les lancer.
- ```build-dev-docker-containers.bat``` pour (re)construire les images des environnements de **DEV** et les lancer.

Et si quelque chose ne fonctionne pas, roulez-vous en boule, dites que vous avez une crise d'appendicite.  
*(ou regardez dans le repository les noms des personnes ayant préalablement contribué au projet et contactez-les ^^)*

_________________


## 🔱 4. Git // Repository et workflow

### 🌍 URLs

Le projet est dans le Gitlab global du groupe, à l'adresse web : 
[https://gitlab-projets.neo-soft.fr/3H-05819](https://gitlab-projets.neo-soft.fr/3H-05819).

Pour y accéder, il vous faudra utiliser les identificants réseau SSO, issus du LDAP Néo-Soft. 
Il vous faudra également être invité(e) ou demander à être ajouté(e) au projet 
(en vous rendant sur l'URL ci-desssus et en utilisant le bouton "*Demander un accès*").
Vous devez être avec le profil "Developer" ou plus pour pouvoir pousser vos modifications.

### 📁 Structure du repository

- **master** sert pour le déploiement en production, et qui validé
- **qa** sert aux différentes étapes de recette des responsables projet, 
et doit aller sur le serveur de recette. Cette branche n'est pas dans le flux des fusions des commits, 
elle est utilisée pour l'intégration continue et la recette.
- Toutes les branches pour travailler doivent être fourchées à partir de **master**, sous la forme :
    * **[jira-id]** pour les branches correspondant à une issue dans Jira (en minuscules)
    * **wip-[raison]** pour les branches correspondant à tout autre besoin (ex: un spike de R&D sur une techno, etc.)
    
- **ci-xxxxxx** servent aux différentes étapes d'intégration continue automatisées, 
n'existent que le temps d'exécution des tests

Ce qui donne l'arborescence suivante :

```
X----------------------------------------------------------------------------------------- master        
         \         \        \        \        \        \
          \         \        \        \        \        *--------------------------------- [jira-id-01]
           \         \        \        \        \
            \         \        \        \        *---------------------------------------- [jira-id-02]
             \         \        \        \
              \         \        \        *----------------------------------------------- [jira-id-03]
               \         \        \
                \         \        *------------------------------------------------------ [jira-id-04]
                 \         \
                  \         *------------------------------------------------------------- wip-[raison]
                   \
                    *--------------*------*------*------*--------------------------------- qa
```


### 🛠 Méthode de travail

#### ➕ Créer une branche : 

- `git checkout master` (on part sur la branche de référence)
- `git checkout -b [jira-id]` (on crée une branche unique pour le PBI de Jira correspondant et on part dessus)
- _(bosser un peu =^^=)_
- `git commit -m "[jira-id]: [[composant]] [explication]"` (on crée le seul et unique commit)
  Note : On peut créer plusieurs commits, pour faire des sauvegardes par ex, ou pour découper notre travail local. 
  Cependant on s'assurera de fusionner (fixup, squash...) en un seul commit pour faire la PR.
- _(bosser encore un peu =^^=)_
- `git commit --amend` (qui propose de modifier le message de commit, en plus)
- `git push origin [jira-id] --force-with-lease` (on sauvegarde ses modifs sur le remote 
  en s'assurant que personne d'autre n'a bossé desssus, si on est sûr.e : `-f`)
  **(NB: CECI NE VAUT QUE SI VOUS ÊTES SEUL.E A TRAVAILLER SUR VOTRE BRANCHE)**
- 

#### ◾ Quelques compléments :

- **Un seul commit par branche** (`git rebase -i xxxxxx` pour réécrire l'historique si besoin),
- TOUTES les PRs doivent avoir **un élément Jira et un seul** associé (id-jira)
- TOUTES les branches doivent passer par une **pull request** quand elles sont OK
  pour la/les personnes qui travaillent dessus.
- TOUTES les pull requests ne doivent avoir qu'**un seul commit**
- AVANT de créer la pull request, bien garder en tête (et de manière fréquente) 
  de faire un **`git rebase`** sur la branche **master**,
  - soit *localement* avec :
    - `git checkout [qo-XX]` (se mettre sur sa branche à fusionner)
    - `git commit --amend` (ne pas perdre ses modifs et vérifier qu'on a tout envoyé, toujours avec un seul commit)
    - `git push` (mettre à jour la branche actuelle, pull avant pour vérifier si on n'est pas seul-e sur sa branche)
    - `git checkout master` (on part sur la branche de référence)
    - `git fetch --all` (on met à jour toutes les branches distantes)
    - `git pull master` (pour être sûr-e d'avoir les modifs localement en working directory, on peut vouloir les voir)
    - `git checkout [qo-XX]` (on repart sur sa branche à fusionner)
    - `git rebase master` (on empile son commit unique sur le reste de la branche de référence mise à jour ci-dessus)
    - `git push --force` (si vous êtes seul-e à travailler sur cette branche, sinon sans le --force)
    
  - soit *directement* à partir de la branche distante (si le contenu de master vous importe peu) :
    - `git checkout [qo-XX]` (se mettre sur sa branche à fusionner)
    - `git commit --amend` (ne pas perdre ses modifs et vérifier qu'on a tout envoyé, toujours avec un seul commit)
    - `git fetch --all` (on met à jour toutes les branches distantes)
    - `git rebase origin/master` (on empile son commit unique sur le reste de la branche de référence 
    mise à jour ci-dessus, mais en prenant l'historique du remote)

 - Soit le travail est une tâche continue, la branche doit s'appeler "**wip-[raison-en-minuscules]**", 
   et le commit unique doit être sous la forme : "**WIP: [[composant]] [explication]**", 
   par exemple : "**WIP: [logs] nettoyage des logs les plus rencontrés**". 
   Elle ne sera pas fusionnée dans QA, ni ailleurs. 
 - Soit le travail est lié à un PBI (un élément dans Jira), 
   la branche doit s'appeler "**[id-jira]**" (ex: "**qo-153**"), 
   et le commit unique doit être sous la forme : "**[clé Jira]: [[composant]] [explication]**", 
   par exemple : "**QO-153: [users] cases à cocher en masse sur les listes**"
 - Éventuellement, quand le contenu du commit correspond à ce qui est à faire dans le PBI Jira, 
 créer une **pull request** dans Gitlab.
 
#### 〰 Workflow de code simplifié et automatisé : 

- En local :
  - **Fork** à partir de master (après l'avoir mis à jour depuis origin bien sûr) pour un PBI nécessitant du code
  - **Travail local**, avec `git rebase` **réguliers** de master sur la branche local 
  (après avoir mis à jour 'master' sur son local...)
  - Création de **pull/merge request** après auto-validation, avec descriptif simplifié, 
    sur la base d’un seul commit Git (rebasing + commit amending), 
    penser à faire un dernier `git rebase` avant de créer la PR.
  - Lancer un déploiement selon l'environnement...
  
#### 🖥 Déploiement 

- Déploiement sur QA 
  - **Collecte** par QA des branches avec pull/merge requests ouvertes et rebasing sur Master
  - **Validation** des pull/merge requests suite à acceptation de code review de l’équipe (dès que possible) 
    et des tests unitaires (basés sur des hooks git, donc permanents), 
    de la code quality et de la revue du créateur de la demande.
  - Si tests utilisateurs (idéalement fait par un autre développeur ou responsable projet) OK, 
    alors on **merge la PR** (et on supprime la branche distante et locale). 
    Les PRs suivantes devront sûrement faire un rebase par rapport à master.
  - Si ok, on **déploie** sur le serveur d'intégration NS la branche `qa` par docker.
  - Enfin, lancer simplement le pipeline Jenkins 
  [3H05819-Qosmos-Deployment-QA](https://forge-ic.neo-soft.fr/jenkins/job/3H05819-Qosmos-Deployment-QA).
  
- Déploiement en prod.
  - Lancer simplement le pipeline Jenkins 
  [3H05819-Qosmos-Deployment-Prod](https://forge-ic.neo-soft.fr/jenkins/job/3H05819-Qosmos-Deployment-Prod).

***N.B.:** le répertoire `/jenkins-jobs` contient une copie des scripts exécutés par la machine de déploiement.  
Ne pas y mettre de données sensibles. ;)* 
 
_________________

  
## ✔ 5. Ecriture de code - standards

### Où travailler sur les sources ?

*Easy PZ*, le projet tente d'adopter le principes de 
[clean coding](https://www.amazon.fr/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882), 
et donc, de manière quasi-transparente :
- **`/backend`** contient les sources du **backend Symfony**
- **`/docker`** contient les **sources Docker** pour la construction des containers/images, 
classées par technologie puis par environnement (ce qui engendre quelques copier/coller inévitables ^^)
- **`/environment-files`** contient les **variables sensibles ou non d'environnement**
- **`/frontend-built`** contient les **sources à déployer** suite à un `ng build` d'Angular
- **`/frontend-sources`** contient les **sources Angular pour travailler** sur le projet
- **`/jenkins-jobs`** contient les **sources des jobs Jenkins** pour la machine de déploiement
- **`.dockerignore`** contient les **éléments à ignorer lors du montage de volumes Docker**
- **`.gitignore`** contient les **éléments à ignorer lors des ajouts à l'index de Git**
- **`build-all-docker-containers`** 
(re)construit les images et (re)lance les containers pour **tous les environnements**
- **`build-prod-docker-containers`** 
(re)construit les images et (re)lance les containers pour **l'environnement de prod**
- **`build-qa-docker-containers`** 
(re)construit les images et (re)lance les containers pour **l'environnement de QA**
- **`build-dev-docker-containers`** 
(re)construit les images et (re)lance les containers pour **l'environnement de dev**
- **`build-dev-docker-containers`** et **`README.md`** 
servent à la génération de cette **documentation projet**, 
que vous êtes en train de lire ! :)


### Outils :

- Pour la partie Symfony, utiliser [PHP CS Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) 
en plus du formatage de l'IDE.
- Pour l'analyse sécurité, utiliser [Symfony Security Checker](https://security.symfony.com/).
- Pour l'analyse statique PHP, utiliser [PHPStan](https://github.com/phpstan/phpstan).

*"GL HF :)"*