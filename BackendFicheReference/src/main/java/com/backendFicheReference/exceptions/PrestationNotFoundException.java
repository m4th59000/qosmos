package com.backendFicheReference.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PrestationNotFoundException extends RuntimeException {

    /**
     * Identifiant de serialisation
     */


    private static final String PRESTATION_NON_TROUVEE = "Prestation non trouvée";
    private static final String PRESTATION_SUPPRIMEE = "Votre prestation a été supprimée";

    public PrestationNotFoundException(String message) {
        super(message);
    }

    public static PrestationNotFoundException prestationNotFound() {
        return new PrestationNotFoundException(PRESTATION_NON_TROUVEE);
    }

    public static PrestationNotFoundException prestationSupprimee() {
        return new PrestationNotFoundException(PRESTATION_SUPPRIMEE);
    }
}

