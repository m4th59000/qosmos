package com.backendFicheReference.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ProjetNotFoundException extends RuntimeException {

    /**
     * Identifiant de serialisation
     */


    private static final String PROJET_NON_TROUVEE = "Projet non trouvée";
    private static final String PROJET_SUPPRIMEE = "Votre projet a été supprimée";

    public ProjetNotFoundException(String message) {
        super(message);
    }

    public static ProjetNotFoundException projetNotFound() {
        return new ProjetNotFoundException(PROJET_NON_TROUVEE);
    }

    public static ProjetNotFoundException projetSupprimee() {
        return new ProjetNotFoundException(PROJET_SUPPRIMEE);
    }
}

