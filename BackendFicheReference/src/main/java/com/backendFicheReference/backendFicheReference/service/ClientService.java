package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.Client;
import org.springframework.data.domain.Page;

public interface ClientService {
    Client findClient(Long id);
    Client saveClient(Client client);
    Client updateClient(Client client , Long id);
    Page<Client> clients(String page, String size, String sortedBy, String sortedIn);
}