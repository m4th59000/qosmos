package com.backendFicheReference.backendFicheReference.service;

import com.backendFicheReference.backendFicheReference.model.Technologie;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface TechnologieService {
    Optional<Technologie> findTechnologie(Long id);
    Technologie saveTechnologie(Technologie technologie);
    Technologie updateTechnologie(Technologie technologie , Long id);
    Page<Technologie> technologies(String page , String size, String sortedBy, String sortedIn);
}
