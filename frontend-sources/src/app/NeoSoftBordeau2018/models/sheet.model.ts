/**
 * Class to materialize a sheet
 */
export class Sheet {

  private _id: number;
  private _name: string;

  private _agence: string;
  private _client: string;
  private _poste: string;

  private _activites: string[];
  private _methodes: string[];
  private _nombreCollaborateurs: string;
  private _technologies: string[];

  private _contextesEtEnjeux: string;
  private _realisations: string;
  private _avantages: string;

  private _selected = false;

  constructor(id: number) {
    this._id = id;
    this._activites = [];
    this._methodes = [];
    this._technologies = [];
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get agence(): string {
    return this._agence;
  }

  set agence(value: string) {
    this._agence = value;
  }

  get client(): string {
    return this._client;
  }

  set client(value: string) {
    this._client = value;
  }

  get poste(): string {
    return this._poste;
  }

  set poste(value: string) {
    this._poste = value;
  }

  get activites(): string[] {
    return this._activites;
  }

  set activites(value: string[]) {
    this._activites = value;
  }

  get methodes(): string[] {
    return this._methodes;
  }

  set methodes(value: string[]) {
    this._methodes = value;
  }

  get nombreCollaborateurs(): string {
    return this._nombreCollaborateurs;
  }

  set nombreCollaborateurs(value: string) {
    this._nombreCollaborateurs = value;
  }

  get technologies(): string[] {
    return this._technologies;
  }

  set technologies(value: string[]) {
    this._technologies = value;
  }

  get contextesEtEnjeux(): string {
    return this._contextesEtEnjeux;
  }

  set contextesEtEnjeux(value: string) {
    this._contextesEtEnjeux = value;
  }

  get realisations(): string {
    return this._realisations;
  }

  set realisations(value: string) {
    this._realisations = value;
  }

  get avantages(): string {
    return this._avantages;
  }

  set avantages(value: string) {
    this._avantages = value;
  }

  get selected(): boolean {
    return this._selected;
  }

  set selected(value: boolean) {
    this._selected = value;
  }
}
