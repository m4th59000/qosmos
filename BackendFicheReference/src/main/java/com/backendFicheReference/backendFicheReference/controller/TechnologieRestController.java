package com.backendFicheReference.backendFicheReference.controller;

import com.backendFicheReference.backendFicheReference.model.Technologie;
import com.backendFicheReference.backendFicheReference.service.TechnologieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TechnologieRestController{
    @Autowired
    private TechnologieService technologieService;
    @RequestMapping(value = "technologies/{id}", method = RequestMethod.GET)
    public Optional<Technologie> findTechnologie(@PathVariable("id") Long id){
        return technologieService.findTechnologie(id);
    }

    @RequestMapping(value = "/technologies", method = RequestMethod.GET)
    public Page<Technologie> listTechnologies(@RequestParam(name = "page" , defaultValue = "0") String page
            , @RequestParam(name = "size" , defaultValue = "10")  String size
            , @RequestParam(name = "sortedBy" , defaultValue = "id")  String sortedBy
            , @RequestParam(name = "sortedIn" , defaultValue = "asc")  String sortedIn){
        return technologieService.technologies(page , size,sortedBy , sortedIn);
    }

    @RequestMapping(value = "/technologies", method = RequestMethod.POST)
    public Technologie findTechnologie(@RequestBody Technologie technologie){
        return technologieService.saveTechnologie(technologie);
    }

    @RequestMapping(value = "/technologies/{id}", method = RequestMethod.PUT)
    public Technologie updateTechnologie(@RequestBody Technologie technologie , @PathVariable("id") @NotNull Long id ){
        return technologieService.updateTechnologie(technologie,id);
    }

}
