import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Prestation} from "../models/Prestation.model";
import {environment} from "../../environments/environment";
import {FicheReferenceAPI} from "../models/FicheReferenceAPI.model";

const baseUrl = environment.endpoint;
const path = 'prestations/';
const headers= new HttpHeaders({
  'Content-type': 'application/json',
  'Access-Control-Allow-Origin': '*'
})
@Injectable({
  providedIn: 'root'
})
export class PrestationService {

  constructor(private _http: HttpClient) { }
  getPrestation(id: string) {
    return this._http.get<Prestation>(baseUrl + path + id , {'headers': headers});
  }
  getPrestations(page: number , size: number , sortedBy: string , sortedIn: string) {

    return this._http.get<FicheReferenceAPI>(baseUrl + path , {'headers': headers,'params': new HttpParams()
        .set('page', ''+page)
        .set('size', ''+size)
        .set('sortedBy', sortedBy)
        .set('sortedIn', sortedIn)}
    );
  }
  createPrestation(prestation: Prestation) {
    return this._http.post<Prestation>(baseUrl + path , prestation , {'headers': headers});
  }
  updatePrestation(prestation: Prestation) {
    return this._http.put<Prestation>(baseUrl + path , prestation , {'headers': headers});
  }
}

