package com.backendFicheReference.backendFicheReference.repository;

import com.backendFicheReference.backendFicheReference.model.PrestationMethodologie;
import com.backendFicheReference.backendFicheReference.model.pk.PrestationMethodologiePk;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PrestationMethodologieRepository extends JpaRepository<PrestationMethodologie, PrestationMethodologiePk> {
    Optional<PrestationMethodologie> findById(PrestationMethodologiePk id);
    Page<PrestationMethodologie> findAll(Pageable pageable);
    List<PrestationMethodologie> findByPrestationId(Long idprestation);
}




