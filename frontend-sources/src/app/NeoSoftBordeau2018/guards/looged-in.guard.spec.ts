import { TestBed, async, inject } from '@angular/core/testing';

import { LoogedInGuard } from './looged-in.guard';

describe('LoogedInGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoogedInGuard]
    });
  });

  it('should ...', inject([LoogedInGuard], (guard: LoogedInGuard) => {
    expect(guard).toBeTruthy();
  }));
});
